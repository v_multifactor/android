#!/bin/sh

# Add certificates to keychain and allow codesign to access them
#security import ./certs/surf_builder.p12 -P "" -T /usr/bin/codesign
security import ./certs/dev_sert.p12 -P "" -T /usr/bin/codesign

output=~/Library/MobileDevice/Provisioning\ Profiles

if [[ ! -e "$output" ]]; then
    mkdir -p "$output"
fi

#cp -R  ./certs/Multifactor_surf_builder_dev.mobileprovision "${output}"
cp -R  ./certs/Multifactor_surf_dev.mobileprovision "${output}"
