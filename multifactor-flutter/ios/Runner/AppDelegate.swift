import UIKit
import Flutter
import Firebase
import flutter_local_notifications


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    

    private var methodChannel: FlutterMethodChannel?
    private var eventChannel: FlutterEventChannel?
    private var requestChannel: FlutterMethodChannel?
    private var ch: FlutterBasicMessageChannel?
    private let linkStreamHandler = LinkStreamHandler()
    private var lastRequest:String=""
    private var inited=false
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      FlutterLocalNotificationsPlugin.setPluginRegistrantCallback { (registry) in
          GeneratedPluginRegistrant.register(with: registry)
      }
      //print("Launch: \(launchOptions)")
      if #available(iOS 10.0, *) {
        //UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
      }

    UNUserNotificationCenter.current().delegate = self
      GeneratedPluginRegistrant.register(with: self)

    let controller = window.rootViewController as! FlutterViewController
    methodChannel = FlutterMethodChannel(name: "poc.deeplink.flutter.dev/channel", binaryMessenger: controller.binaryMessenger)
    eventChannel = FlutterEventChannel(name: "poc.deeplink.flutter.dev/events", binaryMessenger: controller.binaryMessenger)
    requestChannel = FlutterMethodChannel(name: "multifactor.dev/request",binaryMessenger: controller.binaryMessenger)
      ch = FlutterBasicMessageChannel(name: "multifactor.dev/answer",binaryMessenger: controller.binaryMessenger,codec: FlutterStringCodec.sharedInstance())
    methodChannel?.setMethodCallHandler({ (call: FlutterMethodCall, result: FlutterResult) in
      guard call.method == "initialLink" else {
        result(FlutterMethodNotImplemented)
        return
      }
    })
      requestChannel?.setMethodCallHandler({ (call: FlutterMethodCall, result: FlutterResult) in
        guard call.method == "getRequest" else {
          result(FlutterMethodNotImplemented)
          return
        }
          self.getRequestResponse(result: result)
      })

    eventChannel?.setStreamHandler(linkStreamHandler)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
      eventChannel?.setStreamHandler(linkStreamHandler)
      return linkStreamHandler.handleLink(url.absoluteString)
    }
    
    class LinkStreamHandler:NSObject, FlutterStreamHandler {
    
    var eventSink: FlutterEventSink?
    
    // links will be added to this queue until the sink is ready to process them
    var queuedLinks = [String]()
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
      self.eventSink = events
      queuedLinks.forEach({ events($0) })
      queuedLinks.removeAll()
      return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
      self.eventSink = nil
      return nil
    }
    
    func handleLink(_ link: String) -> Bool {
      guard let eventSink = eventSink else {
        queuedLinks.append(link)
        return false
      }
      eventSink(link)
      return true
    }
    }
}

extension AppDelegate {
  override func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    didReceive response: UNNotificationResponse
    //withCompletionHandler completionHandler: @escaping () -> Void
  ) async {
      
      var info=response.notification.request.content.userInfo
      info["response"]=response.actionIdentifier
      response.notification.request.content.setValue(info, forKey: "userInfo")
      //print("Cont:\(response.notification.request.content.userInfo)")
      lastRequest=(info["RequestId"] as? String ?? "") + ":" + (info["AccountId"] as? String ?? "") + ":" + response.actionIdentifier
      print("Request: \(lastRequest)")
      if(inited) {
          let res = await ch?.sendMessage(lastRequest)
          print(res as? String ?? "None")
      }
  }
    private func getRequestResponse(result: FlutterResult) {
        inited=true
        result(lastRequest)
    }
}
