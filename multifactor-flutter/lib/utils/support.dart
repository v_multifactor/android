
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/data/source/remote/remote_data_source.dart';

String getHmacSHA256(String message, String secret) {
  List<int> messageBytes = utf8.encode(message);
  List<int> key = base64.decode(secret);
  Hmac hmac = new Hmac(sha256, key);
  Digest digest = hmac.convert(messageBytes);
  String base64Mac = base64.encode(digest.bytes);
  return base64Mac.trim();
}

Future<bool> approveRequest (String requestId,String accountId,String response) async {

  print("Request1: $requestId,$accountId,$response");
  if(response!="id_1" && response!="id_2") return true;
  final securityStorage = SecureKeyValueStorage.instance();
  var key = await (securityStorage.read(accountId));
  var deviceId = await securityStorage.read("deviceId");
  if(key != null && deviceId != null) {
    var callbackRequest = CallbackRequest();
    callbackRequest.requestId = requestId;
    callbackRequest.accountId = accountId;
    callbackRequest.deviceId = deviceId;
    callbackRequest.sign = (response == "id_1") ?
    getHmacSHA256(
        "Approve:${requestId}:${accountId}:${deviceId}",
        key) :
    getHmacSHA256(
        "Reject:${requestId}:${accountId}:${deviceId}",
        key);
    callbackRequest.action = (response == "id_1") ?
    CallbackRequest.ACTION_APPROVE : CallbackRequest.ACTION_REJECT;
    var remoteDataSource = RemoteDataSource();
    await remoteDataSource.answerAuthRequest(callbackRequest);
    await securityStorage.write("lastProcReq", requestId);
  }
  return true;
}
