import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'key_value_storage.dart';
import 'package:multifactor/data/database/database.dart';

class SecureKeyValueStorage implements KeyValueStorage {
  //final _storage = FlutterSecureStorage(aOptions: AndroidOptions(
  //  encryptedSharedPreferences: true,
  //));
  final _storage = FlutterSecureStorage();
  final _androidOptions = AndroidOptions();
  //final _iosOptions = IOSOptions(accessibility: IOSAccessibility.passcode);
  final _iosOptions = IOSOptions(accessibility:  KeychainAccessibility.first_unlock);

  static KeyValueStorage? _instance;

  static KeyValueStorage instance() {
    if (_instance != null) {
      return _instance!;
    }

    _instance = SecureKeyValueStorage();



    return _instance!;
  }
  @override
  Future<bool> clear() async {
    final prefs = SharedPreferences.getInstance();
    var ver=(await prefs).getString('ver')??'0.0.0';
    var lst=ver.split('.');
    var v=int.parse(lst[0])*10000+int.parse(lst[1])*100+int.parse(lst[2]);
    logger.d("Ver: $v");
    if(v==0) {
      var accounts = await DatabaseProvider.db.getAccounts();
      if(accounts.isNotEmpty) v=1;
    }
    if(v<10008) {
      print(v>0?"REFRESH STORAGE":"CLEAR STORAGE");
      //logger.d(v>0?"REFRESH STORAGE":"CLEAR STORAGE");
      var st=await _storage.readAll(iOptions: _iosOptions, aOptions: _androidOptions);
      await _storage.deleteAll();
      if(v==0) return true;
      for(var v in st.entries) await write(v.key,v.value);
    }
    return false;
  }

  @override
  Future<void> delete(String key) async {
    await _storage.delete(key: key, iOptions: _iosOptions, aOptions: _androidOptions);
  }

  @override
  Future<String?> read(String key) async {
    //logger.d("Key: $key");
    //var st=await _storage.readAll(iOptions: _iosOptions, aOptions: _androidOptions);
    //logger.d("Storage: $st");
    try {
      return await _storage.read(
          key: key, iOptions: _iosOptions, aOptions: _androidOptions);
    } on PlatformException catch (e) {
      // Workaround for https://github.com/mogol/flutter_secure_storage/issues/43
      await _storage.deleteAll();
    }
    return null;

  }

  @override
  Future<void> write(String key, String? value) async {
    await _storage.write(
        key: key,
        value: value,
        iOptions: _iosOptions,
        aOptions: _androidOptions);
  }
}
