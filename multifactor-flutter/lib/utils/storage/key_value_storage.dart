abstract class KeyValueStorage {
  Future<String?> read(String key);

  Future write(String key, String? value);

  Future delete(String key);

  Future clear();
}
