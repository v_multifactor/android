import 'package:multifactor/data/models/account.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/data/models/register_request.dart';

abstract class AccountRepository {
  Future<List<Account>> getAccounts();
  void registerAccount(RegisterRequest registerRequest);
  void approveAuthRequest(CallbackRequest callbackRequest);
  void rejectAuthRequest(CallbackRequest callbackRequest);
  Future<List<AccountRequest>?> getRequests(
      String accountId, String sign, String deviceId);
  Future<void> deleteAccount(String accountId, String sing, String deviceId);
  Future<bool> accountExist(String accountId);
}
