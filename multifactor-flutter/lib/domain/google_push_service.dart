
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:multifactor/ui/utils/parser.dart';
import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:is_lock_screen/is_lock_screen.dart';
import 'package:multifactor/utils/support.dart';



@pragma('vm:entry-point')
void notificationTapBackground(NotificationResponse notificationResponse) async {
  // ignore: avoid_print
 print("HUH");
  logger.d('notification(${notificationResponse.id}) action tapped: '
      '${notificationResponse.actionId} with'
      ' payload: ${notificationResponse.payload}');
  if (notificationResponse.input?.isNotEmpty ?? false) {
    // ignore: avoid_print
    logger.d(
        'notification action tapped with input: ${notificationResponse.input}');

  }
  var selected=(notificationResponse.actionId=="id_1")?
    "A:${notificationResponse.payload}":"D:${notificationResponse.payload}";
    final securityStorage = SecureKeyValueStorage.instance();
    await securityStorage.write("selected", selected);
    logger.d("FINISH SELECT");



}
@pragma('vm:entry-point')
Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("BCK");
  await Firebase.initializeApp();

  logger.d('Handling a background message ${message.data}');
  bool lockScreen = false;
  if(!Platform.isIOS) lockScreen = (await isLockScreen())??false;
  logger.d('Lock screen: $lockScreen');


  //default_notification_channel_id
  await setupFlutterNotifications();
  //await flutterLocalNotificationsPlugin.cancelAll();

  final securityStorage = SecureKeyValueStorage.instance();

  if (message.data['Type'] != "Enroll") {
    if(Platform.isIOS) return null;
    var request = parseRequest(message.data);
    var lastRequest = await securityStorage.read("lastreq");
    if (lastRequest != request.requestId) {
      await securityStorage.write("lastreq", request.requestId);
      var key = await (securityStorage
        .read(request.accountId!));
      var deviceId = await securityStorage
        .read("deviceId");
      logger.d("Keys: $key/$deviceId");
      if (key != null && deviceId != null) {
        var params = "${request.requestId}";
        logger.d('Params: $params');
        await securityStorage.write("selected", "none");
        showFlutterNotification(request, params, lockScreen,Platform.localeName);
        for (var c = 0; c < 25; c++) {
          var curSelected = await securityStorage.read("selected");
          logger.d("Cur selected: $curSelected");
          if (curSelected == "A:${request.requestId}" ||
              curSelected == "D:${request.requestId}") {
            await approveRequest(request.requestId!, request.accountId!,
                curSelected == "A:${request.requestId}"?"id_1":"id_2");
            logger.d("END REQUEST");
          }
          if (curSelected != "none") break;
          await Future.delayed(Duration(seconds: 1));
        }
      }
    }
  }
  else {
    logger.d('Reg: ${message.data['RequestId']}');
    var accountBloc= AccountBloc();
    await securityStorage.write("regId", message.data['RequestId']);
    await accountBloc.registerAccount(message.data['RequestId']);
    print("Reg Finished");
  }
}

  late AndroidNotificationChannel channel;
  late AndroidNotificationChannel base_channel;

bool isFlutterLocalNotificationsInitialized = false;
Future<void> requestPermissions() async {

  final AndroidFlutterLocalNotificationsPlugin? androidImplementation =
  flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>();

  final bool? granted = await androidImplementation?.requestPermission();
  logger.d(granted);
}

Future<void> setupFlutterNotifications() async {
  if (isFlutterLocalNotificationsInitialized) {
    return;
  }
  logger.d("INIT");
  print(Platform.localeName);
  channel = const AndroidNotificationChannel(
    'high_importance_channel1', // id
    'High Importance Notifications', // title
    description:
    'This channel is used for important notifications.', // description
    importance: Importance.max,

  );

  base_channel = const AndroidNotificationChannel(
    'default_channel', // id
    'Default  Notifications', // title
    description:
    'Default channel.', // description
    importance: Importance.none,

  );
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  final NotificationAppLaunchDetails? notificationAppLaunchDetails= await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  var act=notificationAppLaunchDetails?.notificationResponse?.actionId;
  if(act=="id_1") {
    final securityStorage = SecureKeyValueStorage.instance();
    await securityStorage.write("approvedId", notificationAppLaunchDetails?.notificationResponse?.payload??"");
  }
  final securityStorage = SecureKeyValueStorage.instance();
  final lang=Platform.localeName;
  print("Lang: $lang");
  final List<DarwinNotificationCategory> darwinNotificationCategories =
  <DarwinNotificationCategory>[
  DarwinNotificationCategory(
  "main_category",
  actions: <DarwinNotificationAction>[
  DarwinNotificationAction.plain(
      'id_1',
      lang=='ru_RU'?'Подтвердить':'Approve',
      options: <DarwinNotificationActionOption>{
        DarwinNotificationActionOption.authenticationRequired,
      }),
  DarwinNotificationAction.plain(
      'id_2',
      lang=='ru_RU'?'Отклонить':'Deny',
      options: <DarwinNotificationActionOption>{
      DarwinNotificationActionOption.destructive,
  },
  )],options: <DarwinNotificationCategoryOption>{
    DarwinNotificationCategoryOption.allowInCarPlay,
  })];
  final DarwinInitializationSettings initializationSettingsDarwin =     DarwinInitializationSettings(
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
    onDidReceiveLocalNotification:
        (int id, String? title, String? body, String? payload) async {
        logger.d("HMHMHMHM");
    },
    notificationCategories: darwinNotificationCategories,
  );
  final InitializationSettings initializationSettings = InitializationSettings(
      android: AndroidInitializationSettings('@mipmap/logo7'),
      iOS: initializationSettingsDarwin);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    onDidReceiveNotificationResponse: (NotificationResponse resp) {
      logger.d("RESP: ${resp.actionId}");
      if(resp.actionId!="id_1") return;
      final securityStorage = SecureKeyValueStorage.instance();
      securityStorage.write("approvedId", resp.payload??"");
    },
    onDidReceiveBackgroundNotificationResponse: notificationTapBackground);
  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(base_channel);
  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  ///
  //flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
  //    AndroidFlutterLocalNotificationsPlugin>()?.requestPermission();
  //var v= await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
  //   AndroidFlutterLocalNotificationsPlugin>()?.requestPermission();
  //logger.d(v);
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: false,
    badge: false,
    sound: false,
  );
  isFlutterLocalNotificationsInitialized = true;
}

void showFlutterNotification(AccountRequest request,String payload,bool lock,String? lang) {
  //RemoteNotification? notification = message.notification;
  //AndroidNotification? android = message.notification?.android;
  logger.d("LANG: $lang");
  //var request = parseRequest(message.data);
    logger.d("Show Notification");
    flutterLocalNotificationsPlugin.show(
      7219,//message.hashCode,
      request.title,
      request.message,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channelDescription: channel.description,
            icon: '@mipmap/logo7',
            importance: Importance.max,
            priority: Priority.high,
            visibility: NotificationVisibility.public,
            ongoing: false,
            actions: <AndroidNotificationAction>[
            AndroidNotificationAction("id_1",(lang=="ru_RU")?"Подтвердить":"Approve",showsUserInterface: lock),
            AndroidNotificationAction("id_2",(lang=="ru_RU")?"Отклонить":"Deny")
          ]
        ),
        iOS: DarwinNotificationDetails(categoryIdentifier: "main_category" )
      ),

      payload: payload
    );

}

/// Initialize the [FlutterLocalNotificationsPlugin] package.
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;


class GooglePushService {
  AccountBloc accountBloc;
  var isShowing = false;
  var currentAccountId;
  var currentRequest;
  var key = GlobalKey();

  GooglePushService({required this.accountBloc});

  final securityStorage = SecureKeyValueStorage.instance();

  Future initialise(BuildContext context) async {

    logger.d('GooglePushService initialize');

    //await setupFlutterNotifications();
    await getToken();

    //await FirebaseMessaging.instance.requestPermission();

    //FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      print("MSG: ${message.toMap()}");
      logger.d('onMessage: ${message.toMap()}');

      logger.d('Content: ${message.contentAvailable}');


      final data = message.data;

      logger.d("data: $data");
      if (data['Type'] == "Enroll") {
        accountBloc.registerAccount(data['RequestId']);
        bool wakelockStatus = await Wakelock.enabled;
        logger.d("wakelockStatus: $wakelockStatus");
        if (wakelockStatus) {
          Wakelock.disable();
        }
        Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
      } else {
        //var initialMessage=await FirebaseMessaging.instance.getInitialMessage();
        //if(initialMessage==null || parseRequest(initialMessage!.data).requestId!=parseRequest(data).requestId)
        settingModalBottomSheet(context, parseRequest(data));
      }
    });
  }

  Future<void> settingModalBottomSheet(
      BuildContext context, AccountRequest accountRequest) async {
    logger.d("settingModalBottomSheet");
    logger.d(
        "currentAccountId ${currentAccountId} request ${accountRequest.accountId} message ${accountRequest.message}");
    var lastreq=await accountBloc.securityStorage.read("lastProcReq");
    if(lastreq==accountRequest.requestId) return null;
    if (isShowing == true && (currentAccountId != accountRequest.accountId || currentRequest!=accountRequest.requestId)) {
      currentRequest=null;
      currentAccountId = null;
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      }
    }
    String approvedId=await accountBloc.securityStorage.read("approvedId")??"";
    Timer _timer;
    var duration = accountRequest.ttl != null ? accountRequest.ttl! : 90;
    logger.d("Timer: $duration");
    if(accountRequest.requestId==approvedId) duration=2;
    _timer = Timer.periodic(Duration(seconds: duration), (timer) {
        timer.cancel();
        if (isShowing == true && currentAccountId==accountRequest.accountId && currentRequest==accountRequest.requestId) {
          accountBloc.setAuthRequest(null);
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
        }
    });

    bool isButtonTapped = false;

    var callbackRequest = CallbackRequest();
    callbackRequest.accountId = accountRequest.accountId;
    callbackRequest.requestId = accountRequest.requestId;
    bool isExist = await accountBloc.accountExist(accountRequest.accountId);
    logger.d("Showing: $isShowing");

    if (!isShowing && isExist) {
      await accountBloc.securityStorage.write("lastProcReq", accountRequest.requestId);
      currentAccountId = accountRequest.accountId;
      currentRequest=accountRequest.requestId;
      accountBloc.setAuthRequest(accountRequest);
      isShowing = true;
      if(accountRequest.requestId==approvedId) {
        var key = await (securityStorage
            .read(callbackRequest.accountId!));
        callbackRequest.action =
            CallbackRequest.ACTION_APPROVE;
        callbackRequest.deviceId = await securityStorage
            .read("deviceId");
        callbackRequest.sign = getHmacSHA256(
            "Approve:${accountRequest.requestId}:${accountRequest.accountId}:${callbackRequest.deviceId}",
            key!);
        accountBloc.approveAuthRequest(callbackRequest);
      }
      showModalBottomSheet(
          shape: Platform.isIOS
              ? RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(10.0)))
              : RoundedRectangleBorder(),
          backgroundColor: Colors.white,
          context: context,
          builder: (BuildContext bc) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Platform.isIOS
                      ? SvgPicture.asset(
                          "assets/images/top_arrow_cupertino.svg")
                      : SvgPicture.asset(
                          "assets/images/top_arrow_material.svg"),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
                  child: Text("${accountRequest.message}",
                      style: GoogleFonts.roboto(
                          fontWeight: FontWeight.w700, fontSize: 16.0),
                      textAlign: TextAlign.center),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 12.0, left: 16.0, right: 16.0, bottom: 16.0),
                  child: (accountRequest.requestId==approvedId)?Container(
                    width: MediaQuery.of(context).size.width * 0.5 - 26.0,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(86, 181, 97, 1),
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Icon(
                            Icons.done,
                            color: Colors.white,
                            size: 36.0,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: Text(
                              'main.access_requests.approved'.tr(),
                              style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500)),
                        )
                      ],
                    ),

                  ):Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          if (isButtonTapped == false) {
                            isButtonTapped = true;
                            //var accounts =
                            //    await DatabaseProvider.db.getAccounts();
                            var key = await (securityStorage
                                .read(callbackRequest.accountId!));
                            callbackRequest.action =
                                CallbackRequest.ACTION_APPROVE;
                            callbackRequest.deviceId = await securityStorage
                                .read("deviceId");
                            callbackRequest.sign = getHmacSHA256(
                                "Approve:${accountRequest.requestId}:${accountRequest.accountId}:${callbackRequest.deviceId}",
                                key!);
                            accountBloc.approveAuthRequest(callbackRequest);
                            isShowing = false;
                            currentAccountId = null;
                            accountBloc.setAuthRequest(null);
                            if (_timer.isActive == true) {
                              _timer.cancel();
                            }
                            Navigator.pop(context);
                          } else
                            return;
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.5 - 26.0,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(86, 181, 97, 1),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Icon(
                                  Icons.done,
                                  color: Colors.white,
                                  size: 36.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6.0),
                                child: Text(
                                    'main.access_requests.dialog.allow'.tr(),
                                    style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {

                          if (isButtonTapped == false) {
                            isButtonTapped = true;
                            //var accounts =
                            //    await DatabaseProvider.db.getAccounts();
                            var key = await (securityStorage
                                .read(callbackRequest.accountId!));
                            callbackRequest.action =
                                CallbackRequest.ACTION_REJECT;
                            callbackRequest.deviceId = await securityStorage
                                .read("deviceId");
                            callbackRequest.sign = getHmacSHA256(
                                "Reject:${accountRequest.requestId}:${accountRequest.accountId}:${callbackRequest.deviceId}",
                                key!);
                            accountBloc.rejectAuthRequest(callbackRequest);
                            accountBloc.setAuthRequest(null);
                            if (_timer.isActive == true) {
                              _timer.cancel();
                            }
                            Navigator.pop(context);
                          } else
                            return;
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.5 - 26.0,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 126, 133, 1),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Icon(
                                  Icons.close,
                                  color: Colors.white,
                                  size: 36.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6.0),
                                child: Text(
                                    'main.access_requests.dialog.deny'.tr(),
                                    style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          }).whenComplete(() => {isShowing = false, currentAccountId = null});
    }
  }



  Future getToken() async {
    String? token = await (FirebaseMessaging.instance.getToken());
    logger.d('firebaseDeviceToken $token');
    print(token);
    await accountBloc.setDeviceToken(token);
  }
}
