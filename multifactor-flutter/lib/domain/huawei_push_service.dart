import 'dart:async';
import 'dart:io';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:huawei_push/huawei_push.dart';
import 'package:multifactor/data/database/database.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'dart:convert';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';
import 'package:wakelock/wakelock.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:multifactor/ui/utils/parser.dart';
import 'package:multifactor/utils/support.dart';
import 'package:is_lock_screen/is_lock_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:multifactor/domain/google_push_service.dart';


class HuaweiPushService {
  AccountBloc accountBloc;
  var isShowing = false;
  var currentAccountId;
  var currentRequestId;

  HuaweiPushService({required this.accountBloc});

  Future<void> initPlatformState(BuildContext? context) async {
    Push.turnOnPush();

    Push.getTokenStream.listen((event) {
      logger.d('Token received: $event');
      accountBloc.setDeviceToken(event);
    });

    Push.onMessageReceivedStream.listen((event) {
      _onMessageReceived(event, context);
    });

    Push.registerBackgroundMessageHandler(_backgroundMessageCallback);

    Push.getToken('');
  }

  static void _backgroundMessageCallback(RemoteMessage remoteMessage) async {
    // TODO('Change structure of localNotification')
    await Firebase.initializeApp();
    //String data = remoteMessage.data ?? 'EMPTY';
    print("BG onMessageReceived ${remoteMessage.toMap()}");

    bool lockScreen = (await isLockScreen())??false;
    logger.d('Lock screen: $lockScreen');
    //Push.localNotification({
    //  HMSLocalNotificationAttr.TITLE: '[Headless] DataMessage Received',
    //  HMSLocalNotificationAttr.MESSAGE: data
    //});
    await setupFlutterNotifications();

    final securityStorage = SecureKeyValueStorage.instance();
    var data = jsonDecode(remoteMessage.data!);
    if (data['Type'] != "Enroll") {
      var request = parseRequest(data);
      var lastRequest = await securityStorage.read("lastreq");
      if (lastRequest != request.requestId) {
        await securityStorage.write("lastreq", request.requestId);
        var key = await (securityStorage
            .read(request.accountId!));
        var deviceId = await securityStorage
            .read("deviceId");
        logger.d("Keys: $key/$deviceId");
        if (key != null && deviceId != null) {
          var params = "${request.requestId}";
          logger.d('Params: $params');
          await securityStorage.write("selected", "none");
          showFlutterNotification(request, params, lockScreen,Platform.localeName);
          for (var c = 0; c < 25; c++) {
            var curSelected = await securityStorage.read("selected");
            logger.d("Cur selected: $curSelected");
            if (curSelected == "A:${request.requestId}" ||
                curSelected == "D:${request.requestId}") {
              await approveRequest(request.requestId!, request.accountId!,
                  curSelected == "A:${request.requestId}"?"id_1":"id_2");
              logger.d("END REQUEST");
            }
            if (curSelected != "none") break;
            await Future.delayed(Duration(seconds: 1));
          }
        }
      }
    }
    else {
      logger.d('Reg: ${data['RequestId']}');
      var accountBloc= AccountBloc();
      await securityStorage.write("regId", data['RequestId']);
      await accountBloc.registerAccount(data['RequestId']);
      print("Reg Finished");
    }
  }

  void _onMessageReceived(
      RemoteMessage remoteMessage, BuildContext? context) async {
    print("onMessageReceived ${remoteMessage.toMap()}");
    var data = jsonDecode(remoteMessage.data!);
    if (data != null) {
      logger.d("data: $data");
      if (data["Type"] == "Enroll") {
        accountBloc.registerAccount(data['RequestId']);
        bool wakelockStatus = await Wakelock.enabled;
        logger.d("wakelockStatus: $wakelockStatus");
        if (wakelockStatus) {
          Wakelock.disable();
        }
        Navigator.popUntil(context!, (Route<dynamic> route) => route.isFirst);
      } else {
        settingModalBottomSheet(context, parseRequest(data));
      }

      // showResult("onMessageReceived", "Data: " + data);
    } else {
      // showResult("onMessageReceived", "No data is present.");
    }
  }

  Future<void> settingModalBottomSheet(
      BuildContext? context, AccountRequest accountRequest) async {
    logger.d("settingModalBottomSheet");

    logger.d(
        "currentAccountId ${currentAccountId} request ${accountRequest.accountId} message ${accountRequest.message}");

    if (isShowing == true && (currentAccountId != accountRequest.accountId || currentRequestId!=accountRequest.requestId)) {
      currentAccountId = null;
      currentRequestId=null;
      if (Navigator.canPop(context!)) {
        Navigator.pop(context);
      }
    }
    final securityStorage = SecureKeyValueStorage.instance();
    Timer _timer;
    var duration = accountRequest.ttl != null ? accountRequest.ttl! : 90;
    _timer = Timer.periodic(Duration(seconds: duration), (timer) {
        accountBloc.setAuthRequest(null);
        timer.cancel();
        if (isShowing == true && currentAccountId==accountRequest.accountId && currentRequestId==accountRequest.requestId) {
          if (Navigator.canPop(context!)) {
            Navigator.pop(context);
          }
        }
    });

    bool isButtonTapped = false;

    var callbackRequest = CallbackRequest();
    callbackRequest.accountId = accountRequest.accountId;
    callbackRequest.requestId = accountRequest.requestId;
    bool isExist = await accountBloc.accountExist(accountRequest.accountId);

    if (!isShowing && isExist) {
      currentAccountId = accountRequest.accountId;
      currentRequestId = accountRequest.requestId;
      accountBloc.setAuthRequest(accountRequest);
      isShowing = true;
      showModalBottomSheet(
          shape: RoundedRectangleBorder(),
          backgroundColor: Colors.white,
          context: context!,
          builder: (BuildContext bc) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child:
                      SvgPicture.asset("assets/images/top_arrow_material.svg"),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 20.0),
                  child: Text("${accountRequest.message}",
                      style: GoogleFonts.roboto(
                          fontWeight: FontWeight.w700, fontSize: 16.0),
                      textAlign: TextAlign.center),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 12.0, left: 16.0, right: 16.0, bottom: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          if (isButtonTapped == false) {
                            isButtonTapped = true;
                            //var accounts =
                            //    await DatabaseProvider.db.getAccounts();
                            var key = await (securityStorage
                                .read(callbackRequest.accountId!));
                            callbackRequest.action =
                                CallbackRequest.ACTION_APPROVE;
                            callbackRequest.deviceId = await securityStorage
                                .read("deviceId");
                            callbackRequest.sign = getHmacSHA256(
                                "Approve:${accountRequest.requestId}:${accountRequest.accountId}:${callbackRequest.deviceId}",
                                key!);
                            accountBloc.approveAuthRequest(callbackRequest);
                            isShowing = false;
                            currentAccountId = null;
                            accountBloc.setAuthRequest(null);
                            if (_timer.isActive == true) {
                              _timer.cancel();
                            }
                            Navigator.pop(context);
                          } else
                            return;
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.5 - 26.0,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(86, 181, 97, 1),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Icon(
                                  Icons.done,
                                  color: Colors.white,
                                  size: 36.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6.0),
                                child: Text(
                                    'main.access_requests.dialog.allow'.tr(),
                                    style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {
                          if (isButtonTapped == false) {
                            isButtonTapped = true;
                            //var accounts =
                            //    await DatabaseProvider.db.getAccounts();
                            var key = await (securityStorage
                                .read(callbackRequest.accountId!));
                            callbackRequest.action =
                                CallbackRequest.ACTION_REJECT;
                            callbackRequest.deviceId = await securityStorage
                                .read("deviceId");
                            callbackRequest.sign = getHmacSHA256(
                                "Reject:${accountRequest.requestId}:${accountRequest.accountId}:${callbackRequest.deviceId}",
                                key!);
                            accountBloc.rejectAuthRequest(callbackRequest);
                            accountBloc.setAuthRequest(null);
                            if (_timer.isActive == true) {
                              _timer.cancel();
                            }
                            Navigator.pop(context);
                          } else
                            return;
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.5 - 26.0,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 126, 133, 1),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Icon(
                                  Icons.close,
                                  color: Colors.white,
                                  size: 36.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6.0),
                                child: Text(
                                    'main.access_requests.dialog.deny'.tr(),
                                    style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          }).whenComplete(() => {isShowing = false, currentAccountId = null});
    }
  }
}
