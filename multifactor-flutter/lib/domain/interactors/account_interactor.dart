import 'package:multifactor/data/models/account.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/data/models/register_code_status_model.dart';
import 'package:multifactor/data/models/register_request.dart';
import 'package:multifactor/data/repositories/account_repository_impl.dart';

class AccountInteractor {
  var accountRepository = AccountRepositoryImpl();

  Future<List<Account>> getAccounts() => accountRepository.getAccounts();

  Future<void> registerAccount(RegisterRequest registerRequest) async {
    await accountRepository.registerAccount(registerRequest);
  }

  Future<RegisterCodeStatusModel?> sendRegisterCode(
      String code, String? token, String? pushPlatform) async {
    RegisterCodeStatusModel? res =
        await accountRepository.sendRegisterCode(code, token, pushPlatform);
    return res;
  }

  void approveAuthRequest(CallbackRequest callbackRequest) {
    accountRepository.approveAuthRequest(callbackRequest);
  }

  void rejectAuthRequest(CallbackRequest callbackRequest) {
    accountRepository.rejectAuthRequest(callbackRequest);
  }

  Future<void> deleteAccount(
      String? accountId, String sign, String? deviceId) async {
    await accountRepository.deleteAccount(accountId, sign, deviceId);
  }

  Future<bool> accountExist(String? accountId) async {
    bool result = await accountRepository.accountExist(accountId);
    return result;
  }

  Future<List<AccountRequest>?> getRequests(
      String? accountId, String sign, String? deviceId) async {
    return accountRepository.getRequests(accountId, sign, deviceId);
  }
}
