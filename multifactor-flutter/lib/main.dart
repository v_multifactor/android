import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multifactor/data/models/system_params.dart';
import 'package:multifactor/ui/intro.dart';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:multifactor/domain/google_push_service.dart';
//import 'package:multifactor/domain/huawei_push_service.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';
import 'package:multifactor/utils/support.dart';
import 'package:google_api_availability/google_api_availability.dart';
/*@pragma('vm:entry-point')
Future<void> NikHandler1(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("Handling32 a background message: ${message.messageId}");
}*/


void main() async {
  print("MAIN");
  //FirebaseMessaging.onBackgroundMessage(NikHandler1);
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  await setupFlutterNotifications();
  GooglePlayServicesAvailability playStoreAvailability;

  try {
    playStoreAvailability = await GoogleApiAvailability.instance
        .checkGooglePlayServicesAvailability(false);
  } on PlatformException {
    playStoreAvailability = GooglePlayServicesAvailability.unknown;
  }
  if (playStoreAvailability == GooglePlayServicesAvailability.success ||
      Platform.isIOS) {
    await FirebaseMessaging.instance.requestPermission();
    if (Platform.isIOS) {
      const channel = BasicMessageChannel<String>(
          'multifactor.dev/answer', StringCodec());
      channel.setMessageHandler((String? message) async {
        var map = message?.split(':');
        if (map?.length == 3)
          await approveRequest(map![0], map![1], map![2]);
        return 'Done';
      });
      var platform = MethodChannel('multifactor.dev/request');
      String result = "";
      try {
        result = await platform.invokeMethod('getRequest');
        print("Request: $result");
        if (result != "") {
          var map = result.split(':');
          await approveRequest(map![0], map![1], map![2]);
        }
      } on PlatformException catch (e) {
        result = "Error";
        print("Error native call");
      }
    }
    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  }
  final securityStorage = SecureKeyValueStorage.instance();
  await securityStorage.clear();
  await securityStorage.write("selected","");
  await EasyLocalization.ensureInitialized();
  runApp(EasyLocalization(
      path: 'assets/localization',
      supportedLocales: [Locale('en'), Locale('ru')],
      child: Home()));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final bloc = AccountBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates+[RefreshLocalizations.delegate],
      localeResolutionCallback: (Locale? loc, _) {
        SystemParams.saveLang(loc.toString());
        return loc;
      },
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.light,
          scaffoldBackgroundColor: Color.fromRGBO(240, 244, 247, 1),
          // 240,244,247,1
          primaryColor: Color.fromRGBO(64, 130, 188, 1),
          accentColor: Colors.cyan[600],
          // Define the default font family.
          fontFamily: 'Roboto',
          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            headline1: GoogleFonts.roboto(
                fontSize: 28.0,
                color: Color.fromRGBO(50, 69, 77, 1),
                fontWeight: FontWeight.w700),
            headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Roboto'),
          )),
      home: IntroScreen(),
    );
  }
}
