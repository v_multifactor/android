import 'package:multifactor/data/models/account.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/data/models/register_code_status_model.dart';
import 'package:multifactor/data/models/register_request.dart';
import 'package:multifactor/data/source/local/local_data_source.dart';
import 'package:multifactor/data/source/remote/remote_data_source.dart';
import 'package:multifactor/domain/repositories/account_repository.dart';

class AccountRepositoryImpl extends AccountRepository {
  final RemoteDataSource remoteDataSource = RemoteDataSource();
  final LocalDataSource localDataSource = LocalDataSource();

  @override
  Future<List<Account>> getAccounts() => localDataSource.getAccounts();

  @override
  Future<void> registerAccount(RegisterRequest registerRequest) async {
    await remoteDataSource.registerAccount(registerRequest);
  }

  Future<RegisterCodeStatusModel?> sendRegisterCode(
      String code, String? token, String? pushPlatform) async {
    RegisterCodeStatusModel? res =
        await remoteDataSource.sendRegisterCode(code, token, pushPlatform);
    return res;
  }

  @override
  void approveAuthRequest(CallbackRequest callbackRequest) {
    remoteDataSource.answerAuthRequest(callbackRequest);
  }

  @override
  void rejectAuthRequest(CallbackRequest callbackRequest) {
    remoteDataSource.answerAuthRequest(callbackRequest);
  }

  @override
  Future<void> deleteAccount(
      String? accountId, String sign, String? deviceId) async {
    if(deviceId!=null)
      remoteDataSource.removeAccount(accountId, sign, deviceId);
    await localDataSource.deleteAccount(accountId);
  }

  @override
  Future<bool> accountExist(String? accountId) async {
    bool result = await localDataSource.accountExist(accountId);
    return result;
  }

  @override
  Future<List<AccountRequest>?> getRequests(
      String? accountId, String sign, String? deviceId) {
    return remoteDataSource.getRequests(accountId, sign, deviceId);
  }
}
