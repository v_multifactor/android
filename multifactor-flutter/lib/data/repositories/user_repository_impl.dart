class UserRepositoryImpl {
  UserRepositoryImpl._();

  static final userRepository = UserRepositoryImpl._();

  static UserRepositoryImpl? _userRepository;

  Future<UserRepositoryImpl?> get userRep async {
    if (_userRepository != null) return _userRepository;
    _userRepository = this;
    return _userRepository;
  }

  String PREFERENCE_DEVICE_ID = "";
  final String PREFERENCE_PUSH_ID = "push_id";
  final String PREFERENCE_LAT = "lat";
  final String PREFERENCE_LON = "lon";
  final String PREFERENCE_AUTH_ENABLED = "auth_enabled";
}
