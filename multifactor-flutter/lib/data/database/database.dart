import 'dart:async';

import 'package:multifactor/data/models/account.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'create_table.dart';

class DatabaseProvider {
  DatabaseProvider._();

  static final db = DatabaseProvider._();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDatabase();
    return _database!;
  }

  initDatabase() async {
    var dbDirectory = await getDatabasesPath();
    var path = join(dbDirectory, 'MultifactorDatabase.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      createTables.forEach((name, text) async => await db.execute(text));
    });
  }

  Future<List<Account>> getAccounts() async {
    var db = await database;
    var accounts = <Account>[];
    var result = await db.rawQuery("SELECT * FROM accounts");
    result.forEach((element) {
      var account = Account.fromDB(element);
      accounts.add(account);
    });
    return accounts;
  }

  Future<void> deleteAccount(String? accountId) async {
    var db = await (database);
    await db.rawQuery("DELETE FROM accounts WHERE accountId = '$accountId'");
  }

  Future<bool> accountExist(String? accountId) async {
    var db = await (database);
    bool result;
    var accountRequest = await db
        .rawQuery("SELECT * FROM accounts WHERE accountId = '$accountId'");
    if (accountRequest.isNotEmpty) {
      result = true;
    } else
      result = false;
    return result;
  }

  Future<int> insertData(String nameTable, Map<String, dynamic> data) async {
    var dbClient = await (database);
    var id = await dbClient.insert(
      nameTable,
      data,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }
}
