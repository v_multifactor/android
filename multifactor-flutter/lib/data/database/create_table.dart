Map<String, String> createTables = {
  'accounts': """CREATE TABLE accounts (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
          accountId TEXT,
          identity TEXT,
          scope TEXT,
          key TEXT,
          deviceId TEXT,
          creationDate TEXT,
          UNIQUE(accountId)
       );""",
};
