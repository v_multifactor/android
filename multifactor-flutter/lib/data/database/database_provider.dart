import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'create_table.dart';

class DatabaseProvider {
  DatabaseProvider._();

  static final db = DatabaseProvider._();

  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await initDatabase();
    return _database;
  }

  initDatabase() async {
    var dbDirectory = await getDatabasesPath();
    var path = join(dbDirectory, 'MultifactorDatabase.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      createTables.forEach((name, text) async => await db.execute(text));
    });
  }

  Future<int> insertData(String nameTable, Map<String, dynamic> data) async {
    var dbClient = await (database);
    var id = await dbClient!.insert(
      nameTable,
      data,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }
}
