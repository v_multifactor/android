import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:multifactor/ui/utils/logger.dart';

class NetworkConnection {
  bool? networkConnection;
  late Connectivity _connectivity;
  late StreamSubscription<ConnectivityResult> _subscription;

  checkConnect() async {
    _connectivity = new Connectivity();

    _subscription =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      // new connectivity status
      if (result == ConnectivityResult.none) {
        networkConnection = false;
        Fluttertoast.showToast(
          msg: 'main.connect_off'.tr(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
        );
        logger.d('NO INTERNET');
      } else {
        networkConnection = true;
        logger.d('INTERNET IS TURNED ON');
      }
    });
  }

  getConnect() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      networkConnection = false;
      Fluttertoast.showToast(
        msg: 'main.connect_off'.tr(),
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
      );
    } else {
      networkConnection = true;
    }
  }

  void dispose() {
    _subscription.cancel();
  }
}
