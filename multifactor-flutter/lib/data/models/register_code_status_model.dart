import 'dart:core';

class RegisterCodeStatusModel {
  bool? status;
  String? message;

  RegisterCodeStatusModel({this.status, this.message});
}

RegisterCodeStatusModel RegisterCodeStatusModelFromJson(
    Map<dynamic, dynamic> json) {
  return RegisterCodeStatusModel(
    status: json['success'],
    message: json['message'],
  );
}
