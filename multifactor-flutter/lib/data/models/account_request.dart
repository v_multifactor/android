class AccountRequest {
  String? message;

  String? requestId;

  int? ttl;

  String? accountId;

  String? title;

  String? name;

  Set<String>? accounts;

  AccountRequest(
      {this.requestId,
      this.accountId,
      this.ttl,
      this.message,
      this.title,
      this.name,
      this.accounts});

  factory AccountRequest.fromJson(Map<dynamic, dynamic> json) {
    var res = _AccountRequestFromJson(json);
    return res;
  }

  factory AccountRequest.fromJsonForActual(Map<dynamic, dynamic> json) =>
      _AccountRequestFromJsonForActual(json);

  @override
  String toString() {
    return "message $message requestId $requestId ttl $ttl accountId $accountId title $title accounts $accounts";
  }
}

_AccountRequestFromJson(Map<dynamic, dynamic> json) {
  return AccountRequest(
      message: json['Message'],
      requestId: json['RequestId'],
      ttl: int.parse(json['TTL']),
      //ttl: json['TTL'],
      accountId: json['AccountId'],
      name: json['Name'],
      accounts: Set<String>.from(json['allAccounts']??[]));
  // type: json['Auth'],
}

_AccountRequestFromJsonForActual(Map<dynamic, dynamic> json) {
  return AccountRequest(
      message: json['message'],
      requestId: json['requestId'],
      //ttl: int.parse(json['ttl']),
      ttl: json['ttl'],
      title: json["title"],
      name: json['name'],
      accountId: json['accountId'],
      accounts: Set<String>.from(json['allAccounts']??[]));
}
