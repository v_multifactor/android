import 'package:multifactor/ui/utils/logger.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';

class SystemParams {
  static Future<String> getRequestParams() async {
    final sp = await SharedPreferences.getInstance();
    return 'lang=${sp.get('lang') ?? 'ru'}&ver=${sp.get('ver') ?? (await PackageInfo.fromPlatform()).version}';
  }

  static saveLang(String lang) async {
    final sp = await SharedPreferences.getInstance();
    sp.setString('lang', lang);
    final securityStorage = SecureKeyValueStorage.instance();
    logger.d("Lang: $lang");
    securityStorage.write("lang", lang);
  }

  static saveVer(String ver) async {
    final sp = await SharedPreferences.getInstance();
    sp.setString('ver', ver);

  }
}
