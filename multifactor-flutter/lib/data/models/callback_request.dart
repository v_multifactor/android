class CallbackRequest {
  static final String ACTION_APPROVE = "Approve";
  static final String ACTION_REJECT = "Reject";

  String? requestId; // ID запроса

  String? accountId; // ID аккаунта

  String? deviceId; // идентификатор устройства

  String? action; // действие

  String?
      sign; // base64(HMAC_SHA256("Approve:RequestId:AccountId:DeviceId", Key))
  String? appVersion;
  String? lang;
  CallbackRequest(
      {this.requestId, this.accountId, this.deviceId, this.action, this.sign,this.appVersion,this.lang});

  Map<String, dynamic> toJson() => _CallbackRequestToJson(this);

  _CallbackRequestToJson(CallbackRequest callbackRequest) => <String, dynamic>{
        "RequestId": callbackRequest.requestId,
        "AccountId": callbackRequest.accountId,
        "DeviceId": callbackRequest.deviceId,
        "Action": callbackRequest.action,
        "Sign": callbackRequest.sign,
        "appVersion": callbackRequest.appVersion,
        "lang": callbackRequest.lang
      };

  @override
  String toString() {
    return "requestId: $requestId accountId $accountId deviceId $deviceId action $action sign $sign";
  }
}
