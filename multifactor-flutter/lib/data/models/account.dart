import 'dart:core';

class Account {
  String? accountId;

  String? identity;

  String? scope;

  String? key;

  String? deviceId;

  DateTime? creationDate;

  Account(
      {this.accountId,
      this.identity,
      this.scope,
      this.key,
      this.deviceId,
      this.creationDate});

  factory Account.fromJson(Map<dynamic, dynamic> json) =>
      _AccountFromJson(json);

  Map<String, dynamic> toJson() => _AccountToJson(this);

  Map<String, dynamic> toMap() => _AccountToMap(this);

  factory Account.fromDB(Map<String, dynamic> table) {
    return Account(
        accountId: table['accountId'],
        identity: table['identity'],
        scope: table['scope'],
        key: table['key'],
        deviceId: table['deviceId'],
        creationDate: table['creationDate'] != null
            ? DateTime.parse(table['creationDate'])
            : null);
  }

  @override
  String toString() => "Account <$identity>";

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Account &&
          runtimeType == other.runtimeType &&
          accountId == other.accountId &&
          identity == other.identity &&
          scope == other.scope &&
          key == other.key &&
          deviceId == other.deviceId &&
          creationDate == other.creationDate;

  @override
  int get hashCode =>
      accountId.hashCode ^
      identity.hashCode ^
      scope.hashCode ^
      key.hashCode ^
      deviceId.hashCode ^
      creationDate.hashCode;
}

Account _AccountFromJson(Map<dynamic, dynamic> json) {
  return Account(
      accountId: json['accountId'],
      identity: json['identity'],
      scope: json['scope'],
      key: json['key'],
      deviceId: json['deviceId'],
      creationDate: json['creationDate'] != null
          ? DateTime.parse(json['creationDate'])
          : null);
}

Map<String, dynamic> _AccountToJson(Account instance) => <String, dynamic>{
      'accountId': instance.accountId,
      'identity': instance.identity,
      'scope': instance.scope,
      'key': instance.key,
      'deviceId': instance.deviceId,
      'creationDate': instance.creationDate,
    };

Map<String, dynamic> _AccountToMap(Account instance) => <String, dynamic>{
      'accountId': instance.accountId,
      'identity': instance.identity,
      'scope': instance.scope,
      'key': instance.key,
      'deviceId': instance.deviceId,
      'creationDate': instance.creationDate != null
          ? instance.creationDate!.toIso8601String()
          : null,
    };
