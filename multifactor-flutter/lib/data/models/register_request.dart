import 'dart:core';

class RegisterRequest {
  String? requestId;
  String? platform;
  String? platformVersion;
  String? appVersion;
  String? deviceName;
  String? deviceId;
  String? clientToken;
  String? service;
  String? lang;

  RegisterRequest(
      {this.requestId,
      this.platform,
      this.platformVersion,
      this.appVersion,
      this.deviceName,
      this.deviceId,
      this.clientToken,
      this.service,
      this.lang});

  Map<String, dynamic> toJson() => _RegisterRequestToJson(this);

  _RegisterRequestToJson(RegisterRequest registerRequest) => <String, dynamic>{
        "requestId": registerRequest.requestId,
        "platform": registerRequest.platform,
        "platformVersion": registerRequest.platformVersion,
        "appVersion": registerRequest.appVersion,
        "deviceName": registerRequest.deviceName,
        "deviceId": registerRequest.deviceId,
        "clientToken": registerRequest.clientToken,
        "service": registerRequest.service,
        "lang": registerRequest.lang
      };
}

enum ServiceTypeEnum { googlePushService, huaweiPushService }

// fcm - firebase cloud messaging,
// hpk - huawei push kit

final serviceEnumValues = EnumValues({
  "fcm": ServiceTypeEnum.googlePushService,
  "hpk": ServiceTypeEnum.huaweiPushService
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String>? get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
