import 'dart:convert';
//import 'dart:io';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:multifactor/data/database/database.dart';
import 'package:multifactor/data/models/account.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/data/models/register_code_status_model.dart';
import 'package:multifactor/data/models/register_request.dart';
//import 'package:multifactor/data/models/system_params.dart';
import 'package:multifactor/data/services/network_connection.dart';
import 'package:multifactor/domain/exeptions/exeptions.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:multifactor/ui/utils/parser.dart';
import 'package:easy_localization/easy_localization.dart';
import 'dart:async';
//import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';

const timeout = Duration(seconds: 10);

//const String BASE_URL = 'https://api.multifactor.ru/mobileapp';
const String BASE_URL = "https://api.multifactor.dev/mobileapp";

class RemoteDataSource {
  final securityStorage = SecureKeyValueStorage.instance();
  final networkConnection = NetworkConnection();

  Future<RegisterCodeStatusModel?> sendRegisterCode(
      String code, String? token, String? pushPlatform) async {
    RegisterCodeStatusModel? result;
    await networkConnection.getConnect();
    if (networkConnection.networkConnection == true) {
      try {
        final  ver=(await PackageInfo.fromPlatform()).version;
        final  lang=await securityStorage.read("lang");

        var body = <String, dynamic>{
          "code": code,
          "clientToken": token,
          "service": pushPlatform,
          "appVersion": ver,
          "lang": lang ?? 'ru'
        };
        logger.d("Enroll: ${json.encode(body)}");
        //final params = await SystemParams.getRequestParams();
        var response = await http
            .post(Uri.parse('$BASE_URL/enrollment'),
                headers: {"Content-Type": "application/json"},
                body: json.encode(body))
            .timeout(timeout, onTimeout: (() => throw TimeoutException()));
        if (response.statusCode == 200) {
          result = RegisterCodeStatusModelFromJson(json.decode(response.body));
          return result;
          // return json.decode(response.body)["message"];
        } else {
          result = RegisterCodeStatusModelFromJson(json.decode(response.body));
          result.message = "main.error".tr();
          return result;
        }
      } on Exception catch (_) {
        throw NetworkException();
      }
    }
    return null;
  }

  Future<void> registerAccount(RegisterRequest registerRequest) async {
    var locale = Intl.getCurrentLocale();


    await networkConnection.getConnect();
    if (networkConnection.networkConnection == true) {
      try {
        var lang=await securityStorage.read("lang");
        final  ver=(await PackageInfo.fromPlatform()).version;
        registerRequest.appVersion=ver;
        registerRequest.lang=lang ?? 'ru';
        //final params = 'lang=${lang ?? 'ru'}&ver=${(await PackageInfo.fromPlatform()).version}';
        var response = await http
            .post(Uri.parse('$BASE_URL/register'),
                headers: {"Content-Type": "application/json"},
                body: json.encode(registerRequest.toJson()))
            .timeout(timeout, onTimeout: (() => throw TimeoutException()));

        if (response.statusCode == 200) {
          if (json.decode(response.body)["success"] == true) {
            Fluttertoast.showToast(msg: locale=="ru_RU"?"Аккаунт добавлен":"Account added successfully");


            var account = Account.fromJson(json.decode(response.body)["model"]);
            logger.d("saved key ${account.key}");
            securityStorage.write(account.accountId!, account.key);
            account.key = null;
            securityStorage.write("deviceId", account.deviceId);
            await _insertData(account);
          } else {
            logger.e("success error ${response.body}");
          }
        } else {
          logger.e("error1 ${response.statusCode}");
        }
      } on Exception catch (_) {
        throw NetworkException();
      }
    }
  }

  Future<void> _insertData(Account account) async {
    final listAccount = await DatabaseProvider.db.getAccounts();
    if (!listAccount.contains(account)) {
      await DatabaseProvider.db.insertData("accounts", account.toMap());
    }
  }

  Future<void> removeAccount(
      String? accountId, String sign, String? deviceId) async {
    await networkConnection.getConnect();
    final client = http.Client();
    if (networkConnection.networkConnection == true) {
      try {
        //final params = await SystemParams.getRequestParams();
        final  ver=(await PackageInfo.fromPlatform()).version;
        final  lang=await securityStorage.read("lang");
        final response = await client
            .send(http.Request("DELETE", Uri.parse('$BASE_URL'))
              ..headers["Content-Type"] = "application/json"
              ..body = json.encode(<String, String?>{
                "AccountId": accountId,
                "DeviceId": deviceId,
                "Sign": sign,
                "appVersion": ver,
                "lang": lang ?? 'ru'
              }))
            .timeout(timeout, onTimeout: (() => throw TimeoutException()));
        final respStr = await response.stream.bytesToString();
        if (response.statusCode == 200) {
          if (json.decode(respStr)["success"] == true) {
            Fluttertoast.showToast(msg: 'main.account_removed'.tr());
          } else {
            logger.e("success error $respStr");
          }
        } else {
          logger.e("error2 $respStr");
        }
      } finally {
        client.close();
      }
    }
  }

  Future<void> answerAuthRequest(CallbackRequest callbackRequest) async {
    await networkConnection.getConnect();
    if (networkConnection.networkConnection == true) {
      try {
        final  ver=(await PackageInfo.fromPlatform()).version;
        final  lang=await securityStorage.read("lang");
        callbackRequest.lang=lang ?? "ru";
        callbackRequest.appVersion=ver;
        //final params = 'lang=${lang ?? 'ru'}&ver=${(await PackageInfo.fromPlatform()).version}';
        logger.d("Request: ${callbackRequest.toJson()}");
        var response = await http
            .post(Uri.parse('$BASE_URL/callback'),
                headers: {"Content-Type": "application/json"},
                body: json.encode(callbackRequest.toJson()))
            .timeout(timeout, onTimeout: (() => throw TimeoutException()));
        if (response.statusCode == 200) {
          logger.d("success ${response.body}");
        } else {
          logger.e("error3 ${response.body}");
        }
      } on Exception catch (_) {
        throw NetworkException();
      }
    }

  }

  Future<List<AccountRequest>?> getRequests(
      String? accountId, String sign, String? deviceId) async {
    List<AccountRequest>? result;
    await networkConnection.getConnect();
    if (networkConnection.networkConnection == true) {
      logger.d("accountId: $accountId");
      logger.d("sign: $sign");
      logger.d("deviceId: $deviceId");
      try {
        //final params = await SystemParams.getRequestParams();
        final  ver=(await PackageInfo.fromPlatform()).version;
        final  lang=await securityStorage.read("lang");
        logger.d("Req: ${json.encode(<String, dynamic>{
          "AccountId": accountId,
          "DeviceId": deviceId,
          "Sign": sign,
          "appVersion": ver,
          "lang": lang ?? 'ru'
        })}");

        var response = await http
            .post(Uri.parse('$BASE_URL/requests'),
                headers: {"Content-Type": "application/json"},
                body: json.encode(<String, dynamic>{
                  "AccountId": accountId,
                  "DeviceId": deviceId,
                  "Sign": sign,
                  "appVersion": ver,
                  "lang": lang ?? 'ru'
                }))
            .timeout(timeout, onTimeout: (() => throw TimeoutException()));
        if (response.statusCode == 200) {
          if (json.decode(response.body)["success"] == true) {
            logger.d("RES: ${response.body}");
            result = parseListRequests(response.body);
            return result;
          } else {
            logger.e("success error ${response.body}");
          }
        } else {
          logger.e("error5 ${response.body}");
          if(response.statusCode == 410) return [AccountRequest(accountId: "dead")];
        }
      } on Exception catch (_) {
        throw NetworkException();
      }
    }
    return result;
  }
}
