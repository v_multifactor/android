import 'package:multifactor/data/database/database.dart';
import 'package:multifactor/data/models/account.dart';

class LocalDataSource {
  Future<List<Account>> getAccounts() => DatabaseProvider.db.getAccounts();

  Future<void> deleteAccount(String? accountId) async {
    await DatabaseProvider.db.deleteAccount(accountId);
  }

  Future<bool> accountExist(String? accountId) async {
    bool result = await DatabaseProvider.db.accountExist(accountId);
    return result;
  }
}
