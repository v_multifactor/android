import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ToolAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final AppBar? appBar;
  final bool? disableBack;
  ToolAppBar({Key? key, this.title, this.appBar,this.disableBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
          statusBarBrightness: Brightness.light, // For iOS (dark icons)
        ),
        toolbarHeight: 100,
        leading: (disableBack??false)?null:new IconButton(
          padding: EdgeInsets.only(bottom: 0),
          icon: new Icon(Icons.arrow_back_ios, color: Color(0xff32454D)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          title!,
          style: TextStyle(
            color: Color(0xff32454D),
            fontSize: 20,
            fontWeight: FontWeight.w900,
            fontFamily: "SFPro",
          ),
        ),
        centerTitle: true,
      ),
    );
  }

  @override
  Size get preferredSize =>
      new Size.fromHeight(appBar!.preferredSize.height + 20);
}
