import 'package:flutter/material.dart';
import 'package:flutter_jailbreak_detection/flutter_jailbreak_detection.dart';
import 'package:multifactor/ui/accounts/screenlock/screenlock_screen.dart';

import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:multifactor/ui/settings/password/password_screen.dart';
import 'package:multifactor/ui/settings/password/password_set_screen.dart';

import 'package:multifactor/ui/utils/logger.dart';

import 'accounts/accounts_list_screen.dart';
import 'jailbreak/jailbreak_screen.dart';
import 'package:screen_lock_check/screen_lock_check.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:multifactor/data/models/system_params.dart';

class IntroScreen extends StatelessWidget {
  var accountBloc = AccountBloc();
  Widget main = Scaffold(
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
        ],
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    accountBloc.getUserPassword();
    final jbFuture = FlutterJailbreakDetection.jailbroken;
    accountBloc.getPassword.listen((event) async {
      //if(await accountBloc.securityStorage.clear()) event=null;
      PackageInfo.fromPlatform()
          .then((info) => SystemParams.saveVer(info.version));
      final isScreenLockEnabled = await ScreenLockCheck().isScreenLockEnabled;
      logger.d('Screen lock: $isScreenLockEnabled');
      final isJailbreak = await jbFuture;
      await accountBloc.setPasswordRequired(false);
      var isNeedPass= await accountBloc.getPasswordRequired();
      logger.d('Is pass Need: $isNeedPass');
      //if (isJailbreak) {
      //  Navigator.of(context).pushAndRemoveUntil(
      //      MaterialPageRoute(builder: (context) => JailBreakScreen()),
      //      (Route<dynamic> route) => false);
      //  return;
      //}

      if (!isScreenLockEnabled) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LockScreen()),
            (Route<dynamic> route) => false);
        return;
      }
      if (event != null) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => PasswordScreen()),
                (Route<dynamic> route) => false);
      }
      else if(isNeedPass){
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => PasswordSetScreen(accountBloc: accountBloc,initialSet: true)),
                (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => AccountListScreen()),
            (Route<dynamic> route) => false);
      }
    });
    return main;
  }
}
