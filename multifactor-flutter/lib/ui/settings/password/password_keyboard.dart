import 'package:flutter/material.dart';
import 'package:multifactor/ui/utils/captions.dart';

class PasswordKeyboard extends StatelessWidget {
  TextEditingController? password;
  TextEditingController? password_repeat;
  FocusNode? focusNodePassword;
  FocusNode? focusNodeRepeatPassword;

  PasswordKeyboard(
      {Key? key,
      this.password,
      this.password_repeat,
      this.focusNodePassword,
      this.focusNodeRepeatPassword})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: 15.0)),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("1",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '1';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '1';
                    }
                  },
                ),
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("2",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '2';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '2';
                    }
                  },
                ),
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("3",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '3';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '3';
                    }
                  },
                ),
              ]),
//                Padding(padding: EdgeInsets.only(bottom: 40.0)),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("4",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '4';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '4';
                    }
                  },
                ),
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("5",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '5';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '5';
                    }
                  },
                ),
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("6",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '6';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '6';
                    }
                  },
                ),
              ]),
//                Padding(padding: EdgeInsets.only(bottom: 40.0)),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("7",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '7';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '7';
                    }
                  },
                ),
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("8",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '8';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '8';
                    }
                  },
                ),
                MaterialButton(
                  height: 100,
                  minWidth: 100,
                  child: Center(
                    child: Text("9",
                        style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff32454D),
                            fontFamily: 'SFPro')),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                  highlightColor: Color(0xff003282).withOpacity(0.07),
                  onPressed: () {
                    if (focusNodePassword!.hasFocus) {
                      password!.text = password!.text.toString() + '9';
                    } else if (focusNodeRepeatPassword!.hasFocus) {
                      password_repeat!.text =
                          password_repeat!.text.toString() + '9';
                    }
                  },
                ),
              ]),
//                Padding(padding: EdgeInsets.only(bottom: 40.0)),
        ],
      ),
    );
  }
}
