import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:local_auth/local_auth.dart';
import 'package:multifactor/ui/accounts/accounts_list_screen.dart';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:multifactor/ui/widgets/password_board.dart';
import 'package:multifactor/ui/widgets/password_dots_widget.dart';
import 'package:multifactor/ui/settings/password/password_set_screen.dart';

class PasswordScreen extends StatefulWidget {
  bool? requestPasswordDelete = false;
  bool? canDeletePassword = true;

  PasswordScreen({Key? key, this.requestPasswordDelete,this.canDeletePassword}) : super(key: key);

  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  final bloc = AccountBloc();
  TextEditingController pin = new TextEditingController();
  FocusNode? focusNodePin;
  BiometricType? currentBiometric;
  List<BiometricType>? listOfBiometrics;
  bool? currentBiometricPermission;

  int badPasswordCount = 0;
  bool isAvailablePress = true;

  static const int DEFAULT_TIME_DELAY = 5;
  static const int DEFAULT_TRY_LIMIT = 3;
  Timer? timer;

  final LocalAuthentication _localAuthentication = LocalAuthentication();

  @override
  void initState() {
    getListOfBiometricTypes();
    getCurrentBiometricPermission();
    super.initState();
    focusNodePin = FocusNode();
    pin.addListener(() {
      final text = pin.text.toLowerCase();
      pin.value = pin.value.copyWith(
        text: text,
        selection:
            TextSelection(baseOffset: text.length, extentOffset: text.length),
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
    pin.dispose();
    timer?.cancel();
  }

  runTimer() {
    if (!isAvailablePress) {
      return;
    }
    isAvailablePress = false;

    logger.d("Start timer");
    timer = Timer(Duration(seconds: DEFAULT_TIME_DELAY), () {
      setState(() {
        isAvailablePress = true;
        logger.d("Timer finish work.");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var route = ModalRoute.of(context);
    void handler(status) {
      if (status == AnimationStatus.completed) {
        logger.d('Animation completed !');
        authWhenStart();
        route?.animation!.removeStatusListener(handler);
      }
    }

    route?.animation!.addStatusListener(handler);
    return Scaffold(
      body: FutureBuilder<String?>(
          future: bloc.getUserPassword(),
          builder: (context, AsyncSnapshot<String?> snapshot) {
            if (snapshot.hasData) {
              final double height = MediaQuery.of(context).size.height;

              if (snapshot.data == pin.text.toString()) {
                _goToAccountList();
              } else {
                if (snapshot.data!.length == pin.text.toString().length) {
                  _incorrectPassword();
                }
              }

              return Container(
                height: height,
                child: Column(children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                          padding: EdgeInsets.only(
                              top: (height) / 20, bottom: (height) / 5)),
                      Text('access_page.title'.tr(),
                          style: TextStyle(
                              color: Color(0xff32454D),
                              fontSize: 20,
                              fontFamily: "SFPro",
                              fontWeight: FontWeight.w700)),
                      PasswordDotsWidget(
                          count: snapshot.data!.length,
                          selectedCount: pin.text.toString().length),
                      Padding(padding: EdgeInsets.only(bottom: (height) / 15)),
                    ],
                  ),
                  PasswordKeyboard(
                    onButtonClicked: _keyboardClicked,
                    cancelText: widget.requestPasswordDelete == true
                        ? 'access_page.back'.tr()
                        : 'access_page.exit'.tr(),
                    itemHeight: height / 10,
                    width: 85,
                    biometricType: currentBiometricPermission == true
                        ? currentBiometric
                        : null,
                    isShowBack: pin.text.length > 0,
                    isEnabled: isAvailablePress,
                  )
                ]),
              );
            }

            if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }

            return Center(child: CircularProgressIndicator());
          }),
    );
  }

  void _goToAccountList() {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      pin.text = '';
      if (widget.requestPasswordDelete == true && widget.canDeletePassword==true) {
        Navigator.pop(context, true);
      }
      else if(widget.requestPasswordDelete == true && widget.canDeletePassword==false) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
              return PasswordSetScreen(accountBloc: bloc);
            }));
      }
      else {
        logger.d("navigate from pincode input");
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
          return AccountListScreen();
        }));
      }
    });
  }

  void _incorrectPassword() {
    badPasswordCount++;
    if (badPasswordCount >= DEFAULT_TRY_LIMIT) {
      runTimer();
    }
    Fluttertoast.showToast(
      msg: 'main.password_incorrect'.tr(),
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
    );
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      setState(() {
        pin.text = "";
      });
    });
  }

  _keyboardClicked(BtnTypes types, String content) {
    switch (types) {
      case BtnTypes.NUMBER:
        _numberBtnClicked(content);
        break;
      case BtnTypes.BIOMETRIC:
        _biometricBtnClicked();
        break;
      case BtnTypes.CANCEL:
        _navBackOrExit();
        break;
      case BtnTypes.BACKSPACE:
        _backspaceBtnClicked();
        break;
    }
  }

  void _biometricBtnClicked() async {
    if (currentBiometricPermission != false && currentBiometric != null) {
      if (await isBiometricAvailable()) {
        await getCurrentBiometricPermission();
        await getListOfBiometricTypes();
        await authenticateUser();
      }
    }
  }

  void _backspaceBtnClicked() {
    if (pin.text.length > 0) {
      setState(() {
        pin.text = pin.text.substring(0, pin.text.length - 1);
      });
    }
  }

  void _numberBtnClicked(String content) {
    setState(() {
      pin.text = pin.text.toString() + content;
    });
  }

  void _navBackOrExit() {
    if (Navigator.canPop(context)) {
      Navigator.of(context).pop();
    } else {
      exit(0);
    }
  }

  void authWhenStart() async {
    logger.d("version ${Platform.version}");

    bool permission = await bloc.biometricsGetPermission();

    if (permission == true) {
      if (await isBiometricAvailable()) {
        await getCurrentBiometricPermission();
        await getListOfBiometricTypes();
        authenticateUser();
      }
    }
  }

  Future<bool> isBiometricAvailable() async {
    bool isAvailable = false;
    try {
      isAvailable = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      logger.e(e);
    }

    if (!mounted) return isAvailable;

    isAvailable
        ? logger.d('Biometric is available!')
        : logger.d('Biometric is unavailable.');

    return isAvailable;
  }

  Future<void> getListOfBiometricTypes() async {
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      logger.e(e);
    }
    if (!mounted) return;
    logger.d(listOfBiometrics);
    if (listOfBiometrics!.length != 0) {
      currentBiometric = listOfBiometrics!.first;
    }
  }

  Future<void> getCurrentBiometricPermission() async {
    currentBiometricPermission = await bloc.biometricsGetPermission();
  }

  Future<void> authenticateUser() async {
    bool isAuthenticated = false;
    try {
      isAuthenticated = await _localAuthentication.authenticate(
        options: const AuthenticationOptions(
          biometricOnly: true,
          sensitiveTransaction: false,
          stickyAuth: true,
        ),
        authMessages: <AuthMessages>[
          AndroidAuthMessages(
            biometricHint: "",
            signInTitle: 'access_page.title'.tr(),
            cancelButton: 'access_page.cancel'.tr(),
          ),
        ],
        localizedReason: 'settings.fingerprint.fingerprint_message'.tr(),
      );
    } on PlatformException catch (e) {
      logger.e(e);
    }

    if (isAuthenticated) {
      if (widget.requestPasswordDelete == true) {
        // widget.requestPasswordDelete = false;
        Navigator.pop(context, true);
      } else {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => AccountListScreen()),
            (Route<dynamic> route) => false);
      }
    }
  }
}
