import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:multifactor/ui/toolbar.dart';
import 'package:multifactor/ui/utils/captions.dart';
import 'package:multifactor/ui/toolbar.dart';

import 'package:multifactor/ui/accounts/accounts_list_screen.dart';

import '../../bloc/account_bloc.dart';
import '../settings_screen.dart';
import 'package:easy_localization/easy_localization.dart';

class PasswordSetScreen extends StatefulWidget {
  final AccountBloc? accountBloc;
  final bool? initialSet;
  PasswordSetScreen({Key? key, this.accountBloc,this.initialSet}) : super(key: key);

  @override
  _PasswordSetScreenState createState() => _PasswordSetScreenState();
}

class _PasswordSetScreenState extends State<PasswordSetScreen> {
//  var accountBloc = AccountBloc();
  final _formKey = GlobalKey<FormState>();

  TextEditingController _password = new TextEditingController();
  TextEditingController _password_repeat = new TextEditingController();
  FocusNode? focusNodePassword;
  FocusNode? focusNodeRepeatPassword;

  @override
  void initState() {
    super.initState();
    focusNodePassword = FocusNode();
    focusNodeRepeatPassword = FocusNode();
    _password.addListener(() {
      final text = _password.text.toLowerCase();
      _password.value = _password.value.copyWith(
        text: text,
        selection:
            TextSelection(baseOffset: text.length, extentOffset: text.length),
//        composing: TextRange.empty,
      );
    });
    _password_repeat.addListener(() {
      final text = _password_repeat.text.toLowerCase();
      _password_repeat.value = _password_repeat.value.copyWith(
        text: text,
        selection:
            TextSelection(baseOffset: text.length, extentOffset: text.length),
//        composing: TextRange.empty,
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
    _password.dispose();
    _password_repeat.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ToolAppBar(title: 'settings.password.set_password'.tr(), appBar: AppBar(),disableBack: widget.initialSet),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: (MediaQuery.of(context).size.height) / 30,
                left: 20.0,
                right: 20.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Center(
                      child: Text(
                    'settings.password.title'.tr(),
                    style: TextStyle(
                        fontFamily: "SFPro",
                        fontSize: 16,
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.center,
                  )),
                  Padding(
                      padding: EdgeInsets.only(
                          top: (MediaQuery.of(context).size.height) / 40)),
                  TextFormField(
                      decoration: InputDecoration(
                        hintText: 'settings.password.password_prompt'.tr(),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff4082BC)),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Color(0xff4082BC).withOpacity(0.3)),
                        ),
                      ),
                      style: TextStyle(
                          fontFamily: "SFPro",
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                      maxLength: 16,
                      focusNode: focusNodePassword,
                      obscureText: true,
                      autofocus: true,
                      controller: _password,
                      readOnly: true,
                      showCursor: true,
                      validator: (value) {
                        if (value.toString().length < 4) {
                          return 'settings.password.validation_length'.tr();
                        } else if (value.toString().length > 16) {
                          return 'settings.password.validation_big'.tr();
                        }
                        return null;
                      },
                      onTap: () {
                        focusNodePassword!.requestFocus();
                      }),
//                  Padding(padding: EdgeInsets.only(top: (MediaQuery.of(context).size.height) /
//                      30)),
                  TextFormField(
                      decoration: InputDecoration(
                        hintText:
                            'settings.password.repeat_password_prompt'.tr(),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff4082BC)),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Color(0xff4082BC).withOpacity(0.3)),
                        ),
                      ),
                      style: TextStyle(
                          fontFamily: "SFPro",
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                      maxLength: 16,
                      focusNode: focusNodeRepeatPassword,
                      obscureText: true,
                      controller: _password_repeat,
                      readOnly: true,
                      showCursor: true,
                      validator: (value) {
                        if (value.toString() != _password.text.toString()) {
                          return 'settings.password.validation_match'.tr();
                        }
                        return null;
                      },
                      onTap: () {
                        focusNodeRepeatPassword!.requestFocus();
                      }),
                  Padding(
                      padding: EdgeInsets.only(
                          top: (MediaQuery.of(context).size.height) / 30)),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              children: [
//                Spacer(),
                Padding(
                    padding: EdgeInsets.only(
                        top: (MediaQuery.of(context).size.height) / 50)),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("1",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '1';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '1';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("2",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '2';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '2';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("3",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '3';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '3';
                          }
                        },
                      ),
                    ]),
//                Padding(padding: EdgeInsets.only(bottom: 40.0)),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("4",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '4';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '4';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("5",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '5';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '5';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("6",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '6';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '6';
                          }
                        },
                      ),
                    ]),
//                Padding(padding: EdgeInsets.only(bottom: 40.0)),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("7",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '7';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '7';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("8",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '8';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '8';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("9",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '9';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '9';
                          }
                        },
                      ),
                    ]),
//                Padding(padding: EdgeInsets.only(bottom: 40.0)),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Icon(
                          Icons.backspace,
                        ),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus) {
                            if (_password.text.length != 0) {
                              _password.text = _password.text
                                  .toString()
                                  .substring(
                                      0, _password.text.toString().length - 1);
                            }
                          } else if (focusNodeRepeatPassword!.hasFocus) {
                            if (_password_repeat.text.length != 0) {
                              _password_repeat.text = _password_repeat.text
                                  .toString()
                                  .substring(
                                      0,
                                      _password_repeat.text.toString().length -
                                          1);
                            }
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text("0",
                              style: TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff32454D),
                                  fontFamily: 'SFPro')),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (focusNodePassword!.hasFocus &&
                              (_password.text.length < 16)) {
                            _password.text = _password.text.toString() + '0';
                          } else if (focusNodeRepeatPassword!.hasFocus &&
                              (_password_repeat.text.length < 16)) {
                            _password_repeat.text =
                                _password_repeat.text.toString() + '0';
                          }
                        },
                      ),
                      MaterialButton(
                        height: (MediaQuery.of(context).size.height) / 10,
                        minWidth: 85,
                        child: Center(
                          child: Text('settings.password.next_button'.tr(),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "SFPro")),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75)),
                        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
                        highlightColor: Color(0xff003282).withOpacity(0.07),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            widget.accountBloc!
                                .setUserPassword(_password.text.toString());
                            if(widget.initialSet??false)
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                    return AccountListScreen();
                                  }));
                            else
                              Navigator.pop(context);
                            Fluttertoast.showToast(
                              msg: 'settings.password.is_on'.tr(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                            );
                          }
                        },
                      ),
                    ]),
              ],
            ),
          )
        ],
      ),
    );
  }
}
