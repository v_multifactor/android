import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:multifactor/ui/settings/password/password_screen.dart';
import 'package:multifactor/ui/toolbar.dart';
import 'package:multifactor/ui/settings/password/password_set_screen.dart';
import 'package:local_auth/local_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:multifactor/ui/utils/logger.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final bloc = AccountBloc();
  bool? requestPasswordDelete = false;
  bool? passwordExist;
  List<BiometricType> listOfBiometrics = <BiometricType>[];

  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<void> _getListOfBiometricTypes() async {
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      logger.e(e);
    }
    if (!mounted) return;

    logger.d(listOfBiometrics);
  }

  @override
  void initState() {
    bloc.getUserPassword();
    _getListOfBiometricTypes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ToolAppBar(title: 'settings.title'.tr(), appBar: AppBar()),
      body: StreamBuilder<String?>(
        stream: bloc.getPassword,
        builder: (context, AsyncSnapshot<String?> snapshot) {
          if (snapshot.data == null) {
            passwordExist = false;
          } else
            passwordExist = true;
          return Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: ListView(
              children: [
                snapshot.data == null
                    ? Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16.0)),
                          child: InkResponse(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              enableFeedback: true,
                              child: Container(
                                height: 70,
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Row(
                                    children: [
                                      Icon(Icons.lock_outline),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12.0),
                                        child: Text(
                                            'settings.password.set_password'
                                                .tr(),
                                            style: TextStyle(
                                                fontFamily: "SFPro",
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              onTap: () {
                                setState(() {});
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return PasswordSetScreen(accountBloc: bloc);
                                }));
                              }),
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16.0)),
                          child: InkResponse(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              enableFeedback: true,
                              child: Container(
                                height: 70,
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Row(
                                    children: [
                                      Icon(Icons.lock_outline),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12.0),
                                        child: Text((bloc.isNeedPassword??false)?
                                            'settings.password.change_password'.tr():
                                            'settings.password.remove_password'.tr(),
                                            style: TextStyle(
                                                fontFamily: "SFPro",
                                                fontSize: 16,
                                                fontWeight: FontWeight.w400)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              onTap: () {
                                deletePassword(context);
                              }),
                        ),
                      ),
                passwordExist == false
                    ? Opacity(opacity: 0.4, child: activateBiometric(context))
                    : activateBiometric(context)
              ],
            ),
          );

//          else return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  void deletePassword(BuildContext context) async {
    bool? result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PasswordScreen(
              requestPasswordDelete: true, canDeletePassword: (bloc.isNeedPassword??false)?false:true,
            )));
    setState(() {
      requestPasswordDelete = result;

      if (requestPasswordDelete == true && bloc.isNeedPassword==false) {
        Fluttertoast.showToast(
          msg: 'settings.password.delete_password_success'.tr(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
        );
        setState(() {
          bloc.deleteUserPassword(); // Удалить пароль
          bloc.biometricsSetPermission(false); // и также отключить биометрию
        });
      }
    });
  }

  Widget activateBiometric(BuildContext context) {
    late String cardName;
    return FutureBuilder<bool>(
      future: bloc.biometricsGetPermission(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          listOfBiometrics.forEach((element) {
            if (element == BiometricType.fingerprint ||
                element == BiometricType.weak ||
                element == BiometricType.strong) {
              if (snapshot.data == false) {
                cardName = 'settings.fingerprint.use_fingerprint'.tr();
              } else {
                cardName = 'settings.fingerprint.disable_fingerprint'.tr();
              }
            } else if (element == BiometricType.face) {
              if (snapshot.data == false) {
                cardName = 'settings.face_id.use_face_id'.tr();
              } else {
                cardName = 'settings.face_id.disable_face_id'.tr();
              }
            }
          });

          if (listOfBiometrics.isEmpty) {
            return Container();
          } else
            return Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0)),
                child: InkResponse(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Container(
                      height: 70,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          children: [
                            _isHaveFingerprint()
                                ? Icon(Icons.fingerprint)
                                : SvgPicture.asset('assets/images/faceid.svg'),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Text(cardName,
                                    style: TextStyle(
                                        fontFamily: "SFPro",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400)),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    onTap: () async {
                      if (passwordExist == false) {
                        return;
                      } else {
                        if (_isHaveFingerprint()) {
                          if (snapshot.data == false) {
                            _biometricsSetPermission(
                              'settings.fingerprint.is_on_msg',
                              true,
                            );
                          } else {
                            final result = await Navigator.of(context)
                                .push(MaterialPageRoute(
                                    builder: (context) => PasswordScreen(
                                          requestPasswordDelete: true,
                                        )));
                            if (result) {
                              _biometricsSetPermission(
                                'settings.fingerprint.is_off_msg',
                                false,
                              );
                            }
                          }
                        } else if (listOfBiometrics.first ==
                            BiometricType.face) {
                          if (snapshot.data == false) {
                            _biometricsSetPermission(
                              'settings.face_id.is_on_msg',
                              true,
                            );
                          } else {
                            final result = await Navigator.of(context)
                                .push(MaterialPageRoute(
                                    builder: (context) => PasswordScreen(
                                          requestPasswordDelete: true,
                                        )));
                            if (result != null && result) {
                              _biometricsSetPermission(
                                'settings.face_id.is_off_msg',
                                false,
                              );
                            }
                          }
                        }
                      }
                      setState(() {});
                    }),
              ),
            );
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        } else
          return Center(child: CircularProgressIndicator());
      },
    );
  }

  bool _isHaveFingerprint() {
    return listOfBiometrics.contains(BiometricType.fingerprint) ||
        listOfBiometrics.contains(BiometricType.weak) ||
        listOfBiometrics.contains(BiometricType.strong);
  }

  void _biometricsSetPermission(String message, bool isIncluded) {
    bloc.biometricsSetPermission(isIncluded);
    Fluttertoast.showToast(
      msg: message.tr(),
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
    );
  }
}
