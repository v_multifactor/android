import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multifactor/data/models/register_code_status_model.dart';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:wakelock/wakelock.dart';

import '../toolbar.dart';

class EnterCodeScreen extends StatefulWidget {
  String? deviceToken;

  EnterCodeScreen({required this.deviceToken});

  @override
  _EnterCodeScreenState createState() => _EnterCodeScreenState();
}

class _EnterCodeScreenState extends State<EnterCodeScreen> {
  final _formKey = GlobalKey<FormState>();

  // FocusNode keyFocusNode;
  TextEditingController _keyController = new TextEditingController();
  TextEditingController _accountNameController = new TextEditingController();
  var accountBloc = AccountBloc();

  @override
  void initState() {
    // keyFocusNode = FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ToolAppBar(
          title: 'main.camera.add_account_title'.tr(), appBar: AppBar()),
      body: Column(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'main.account.enter_key'.tr(),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff4082BC)),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Color(0xff4082BC).withOpacity(0.3)),
                        ),
                      ),
                      style: TextStyle(
                          fontFamily: "SFPro",
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                      keyboardType: TextInputType.phone,
                      autofocus: true,
                      controller: _keyController,
                      validator: (value) {
                        if (value.toString().length < 6) {
                          return 'main.account.account_key_length'.tr();
                        }
                        return null;
                      },
                      onTap: () {}),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 12.0, left: 12.0, right: 12.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    color: Color.fromRGBO(64, 130, 188, 0.60),
                    child: MaterialButton(
                      splashColor: Color.fromRGBO(64, 130, 188, 0.70),
                      highlightColor: Color.fromRGBO(64, 130, 188, 0.70),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0)),
                      enableFeedback: true,
                      child: Container(
                        height: 55,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('main.account.add_account'.tr(),
                                  style: TextStyle(
                                      fontFamily: "SFPro",
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white)),
                            ],
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          sendRegisterCode(_keyController.text);
                        }
                      },
                      // onTap: () {
                      //   if (_formKey.currentState.validate()) {
                      //     sendRegisterCode(_keyController.text);
                      //   }
                      // }
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> sendRegisterCode(String code) async {
    // String androidPushPlatform = await accountBloc.getAndroidPushPlatform();
    // String devicePushToken;

    // print("serviceEnumValues.reverse[ServiceTypeEnum.googlePushService]: ${serviceEnumValues.reverse[ServiceTypeEnum.googlePushService]}");
    // if(androidPushPlatform == serviceEnumValues.reverse[ServiceTypeEnum.googlePushService]) {
    //   final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    //   devicePushToken = await _firebaseMessaging.getToken();
    // }
    // else {
    //   HuaweiPushService huaweiService = HuaweiPushService();
    //   huaweiService.getToken();
    //   devicePushToken = "123"; // Потом сюда прокинуть токен
    // }

    logger.d("widget.deviceToken: ${widget.deviceToken}");

    RegisterCodeStatusModel? res =
        await accountBloc.sendRegisterCode(code, widget.deviceToken);
    logger.d("res: ${res?.message}");

    if (res != null) {
      _showCodeConfirmDialog(res);
    }
  }

  void _showCodeConfirmDialog(RegisterCodeStatusModel registerCodeStatus) {
    setState(() {
      Wakelock.enable();
      // You could also use Wakelock.toggle(on: true);
    });
    showDialog(
      barrierDismissible: registerCodeStatus.status == true ? false : true,
      // Если код пришел, отключаем закрытие окна по затемненному полю(правка заказчика)
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(
            "main.camera.add_account_title".tr(),
            style: GoogleFonts.roboto(
                fontStyle: FontStyle.normal,
                fontSize: 22.0,
                fontWeight: FontWeight.w700,
                color: Color.fromRGBO(50, 69, 77, 1)),
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: Text("${registerCodeStatus.message}")),
              Container(
                  margin: EdgeInsets.only(left: 5),
                  width: 15,
                  height: 15,
                  child: registerCodeStatus.status == true
                      ? CircularProgressIndicator()
                      : Container())
            ],
          ),
          actions: <Widget>[
            registerCodeStatus.status == true
                ? new TextButton(
                    child: new Text("main.cancel".tr()),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                : new TextButton(
                    child: new Text("Ok"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
          ],
        );
      },
    );
  }
}
