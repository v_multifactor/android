import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

/// Custom mobile scanner controller for [MobileScanner]. Override the [start]
/// method in order not to make a repeated request for permission to use the camera.
class CustomMobileScannerController extends MobileScannerController {
  @override
  Future<void> start() async {
    ensure('startAsync');
    if (isStarting) {
      throw Exception('mobile_scanner: Called start() while already starting.');
    }
    isStarting = true;

    cameraFacingState.value = facing;

    // Set the starting arguments for the camera
    final Map arguments = {};
    arguments['facing'] = facing.index;
    if (ratio != null) arguments['ratio'] = ratio;
    if (torchEnabled != null) arguments['torch'] = torchEnabled;

    if (formats != null) {
      if (Platform.isAndroid) {
        arguments['formats'] = formats!.map((e) => e.index).toList();
      } else if (Platform.isIOS || Platform.isMacOS) {
        arguments['formats'] = formats!.map((e) => e.rawValue).toList();
      }
    }

    // Start the camera with arguments
    Map<String, dynamic>? startResult = {};
    try {
      startResult = await methodChannel.invokeMapMethod<String, dynamic>(
        'start',
        arguments,
      );
    } on PlatformException catch (error) {
      debugPrint('${error.code}: ${error.message}');
      isStarting = false;
      return;
    }

    if (startResult == null) {
      isStarting = false;
      throw PlatformException(code: 'INITIALIZATION ERROR');
    }

    hasTorch = startResult['torchable'] as bool;

    if (kIsWeb) {
      args.value = MobileScannerArguments(
        webId: startResult['ViewID'] as String?,
        size: Size(
          startResult['videoWidth'] as double,
          startResult['videoHeight'] as double,
        ),
        hasTorch: hasTorch,
      );
    } else {
      args.value = MobileScannerArguments(
        textureId: startResult['textureId'] as int,
        size: _toSize(startResult['size'] as Map),
        hasTorch: hasTorch,
      );
    }

    isStarting = false;
  }
}

Size _toSize(Map data) {
  final width = data['width'] as double;
  final height = data['height'] as double;
  return Size(width, height);
}
