import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:multifactor/ui/accounts/accounts_list_screen.dart';
import 'package:multifactor/ui/accounts/qr_scanner/custom_mobile_scanner_controller.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:easy_localization/easy_localization.dart';

class ScanQrScreen extends StatefulWidget {
  @override
  _ScanQrScreenState createState() => _ScanQrScreenState();
}

class _ScanQrScreenState extends State<ScanQrScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool isAlreadyInProgress = false;
  late CustomMobileScannerController? _controller;

  @override
  void initState() {
    super.initState();
    _controller = CustomMobileScannerController();
    _requestPermission();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      extendBody: true,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            MobileScanner(
              controller: _controller,
              key: qrKey,
              onDetect: _onQRViewCreated,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 120,
              color: Color.fromRGBO(0, 0, 0, 0.8),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 170,
                color: Color.fromRGBO(0, 0, 0, 0.8),
                child: Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Text('main.camera.add_account_copy'.tr(),
                      style: GoogleFonts.roboto(
                          height: 1.4,
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 70.0,
                left: 22.0,
                right: 20.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('main.camera.add_account_title'.tr(),
                      style: GoogleFonts.roboto(
                          color: Colors.white,
                          fontSize: 22.0,
                          fontWeight: FontWeight.w500)),
                  MaterialButton(
                    minWidth: 0,
                    padding: EdgeInsets.only(top: 0),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: CircleBorder(),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: SizedBox(
                      width: 30.0,
                      height: 30.0,
                      child: Icon(Icons.close, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onQRViewCreated(Barcode scanData, _) {
    if (scanData.rawValue != null) {
      logger.d("SCAN DATA = ${scanData.rawValue}");
      if (scanData.rawValue!.substring(0, 11) == "multifactor") {
        if (!isAlreadyInProgress) {
          isAlreadyInProgress = true;

          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) =>
                  AccountListScreen(qrCode: scanData.rawValue),
            ),
            ModalRoute.withName('/'),
          );
        }
      }
    }
  }

  void _requestPermission() async {
    var status = await Permission.camera.request();
    logger.d("status $status ");
    if (status == PermissionStatus.permanentlyDenied ||
        status == PermissionStatus.denied) {
      if (Platform.isIOS) {
        showCupertinoDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
                  title: Text('main.camera.access_dialog.title'.tr(),
                      style: TextStyle(letterSpacing: -1.0)),
                  content: Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text('main.camera.access_dialog.copy'.tr()),
                  ),
                  actions: <Widget>[
                    CupertinoDialogAction(
                      isDefaultAction: true,
                      onPressed: () => Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AccountListScreen()),
                          (route) => false),
                      child: Text('main.camera.access_dialog.option_skip'.tr()),
                    ),
                    CupertinoDialogAction(
                      child: Text('main.settings'.tr()),
                      onPressed: () => AppSettings.openAppSettings(),
                    )
                  ],
                ));
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => AccountListScreen()),
            (route) => false);
      }
    }
  }
}
