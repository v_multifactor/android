import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MainAddAccountButton extends StatelessWidget {
  const MainAddAccountButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 96,
      child: Center(
        child: Wrap(
          children: [
            Container(
              width: 28,
              height: 28,
              child: SvgPicture.asset(
                'assets/images/add_icon_material.svg',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
