import 'dart:async';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_api_availability/google_api_availability.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multifactor/data/database/database.dart';
import 'package:multifactor/data/models/account.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/register_request.dart';
import 'package:multifactor/domain/google_push_service.dart';
import 'package:multifactor/domain/huawei_push_service.dart';
import 'package:multifactor/ui/accounts/qr_scanner/scan_qr_screen.dart';
import 'package:multifactor/ui/accounts/widgets/main_add_account_button.dart';
import 'package:multifactor/ui/bloc/account_bloc.dart';
import 'package:multifactor/ui/bloc/deep_link_bloc.dart';
import 'package:multifactor/ui/settings/settings_screen.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:multifactor/utils/cupertino_ink_well.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:screen_lock_check/screen_lock_check.dart';
import 'package:multifactor/ui/accounts/screenlock/screenlock_screen.dart';
import 'package:multifactor/data/services/network_connection.dart';
import 'package:wakelock/wakelock.dart';
import 'package:multifactor/utils/support.dart';
//import 'package:path/path.dart';

import '../settings/settings_screen.dart';
import 'enter_code_screen.dart';

class AccountListScreen extends StatefulWidget {
  String? qrCode;

  AccountListScreen({this.qrCode});

  @override
  _AccountListScreenState createState() => _AccountListScreenState();
}

class _AccountListScreenState extends State<AccountListScreen>
    with WidgetsBindingObserver {
  var accountBloc = AccountBloc();
  final networkConnection = NetworkConnection();
  var _deepLinkBloc = DeepLinkBloc();
  var deadAccs = Set<String>();
  var brokenAccs = Set<String>();

  final securityStorage = SecureKeyValueStorage.instance();

  RefreshController _refreshController = RefreshController();

  GooglePushService? googleService;
  HuaweiPushService? huaweiService;

  ServiceTypeEnum servicePushPlatform =
      ServiceTypeEnum.googlePushService; // default = google

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.d("State: $state");
    print("State: $state");
    print(ModalRoute.of(context)?.isCurrent);

    if(state!=AppLifecycleState.resumed) return;
    accountBloc.securityStorage.write("selected", "");
    //fetchRequests().then((value) => setState(() {
    //  deadAccs=value;
    //}));

    ScreenLockCheck().isScreenLockEnabled.then((value) {
      if(!value) Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LockScreen()),
              (Route<dynamic> route) => false);
      else  fetchRequests().then((value) => setState(() {
        deadAccs=value;
      }));

    });
  }

  @override
  void initState() {

    networkConnection.getConnect();
    networkConnection.checkConnect();
    //accountBloc.securityStorage.write("selected", "");
    checkPlatformsSupport().then((_) {
      WidgetsBinding.instance!.addObserver(this);
      _deepLinkBloc.state.listen((event) {
        registerAccount(event!);
      });
      if (widget.qrCode != null) {
        registerAccount(widget.qrCode!);
      }
    });
    accountBloc.fetchAccounts();
    print("InitState");
    fetchRequests().then((value){
      deadAccs=value;
      accountBloc.fetchAccounts();
    });
    super.initState();
    //requestPermissions();
  }

  Future<void> checkPlatformsSupport([bool showDialog = false]) async {
    GooglePlayServicesAvailability playStoreAvailability;

    try {
      playStoreAvailability = await GoogleApiAvailability.instance
          .checkGooglePlayServicesAvailability(showDialog);
    } on PlatformException {
      playStoreAvailability = GooglePlayServicesAvailability.unknown;
    }
    //playStoreAvailability = GooglePlayServicesAvailability.unknown;

    if (playStoreAvailability == GooglePlayServicesAvailability.success ||
        Platform.isIOS) {
      servicePushPlatform = ServiceTypeEnum.googlePushService;
    } else {
      servicePushPlatform = ServiceTypeEnum.huaweiPushService;
    }
    accountBloc.setAndroidPushPlatform(servicePushPlatform);

    if (!mounted) {
      return;
    }

    googleService = GooglePushService(accountBloc: accountBloc);
    huaweiService = HuaweiPushService(accountBloc: accountBloc);

    if (servicePushPlatform == ServiceTypeEnum.googlePushService ||
        Platform.isIOS) {
      googleService!.initialise(context);
    } else {
      huaweiService!.initPlatformState(context);
    }

  }

  @override
  void dispose() {
    accountBloc.dispose();
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EmptyAppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          _buildHeader(),
          Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Text('main.my_accounts'.tr(),
                  style: Theme.of(context).textTheme.headline1)),
          _buildAccountRequest(),
          _buildAccountsSection()
        ],
      ),
    );
  }

  Widget _buildAccountRequest() {
    return StreamBuilder<AccountRequest?>(
        stream: accountBloc.getAuthRequest,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 6.0, horizontal: 20.0),
              child: GestureDetector(
                child: Card(
                  elevation: 6.0,
                  color: Color(0xff56B561),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                  child: Container(
                    width: double.infinity,
                    height: 96,
                    child: Padding(
                      padding: EdgeInsets.only(left: 5, right: 5),
                      child: Center(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('main.access_requests.request'.tr(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700)),
                          Padding(padding: EdgeInsets.all(4.0)),
                          Text(snapshot.data!.name!,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.w700))
                        ],
                      )),
                    ),
                  ),
                ),
                onTap: () => googleService?.settingModalBottomSheet(
                    context, snapshot.data!),
              ),
            );
          } else if(deadAccs.isNotEmpty) {
            return Padding(
              padding:
              const EdgeInsets.symmetric(vertical: 6.0, horizontal: 20.0),
                child: Card(
                  elevation: 6.0,
                  color: Color(0xffe0e0e0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                  child: Container(
                    width: double.infinity,
                    height: 96,
                    child: Padding(
                      padding: EdgeInsets.only(left: 5, right: 5),
                      child: Center(
                          child: Text(brokenAccs.isNotEmpty?'main.account.broken_account'.tr():'main.account.invalid_account'.tr(),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700))
                          )),
                    ),
                  ),
                );
          }
          else
            return Container();
        });
  }

  Widget _buildHeader() {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.only(top: 14.0),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 50.0,
                    height: 50.0,
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(50.0),
                        ),
                      ),
                      padding: EdgeInsets.all(0),
                      child: SvgPicture.asset(
                        "assets/images/settings_icon.svg",
                        color: Color.fromRGBO(64, 130, 188, 1),
                        width: 24.0,
                        height: 24.0,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SettingsScreen(),
                          ),
                        );
                      },
                    ),
                  )),
            ),
            Center(
                child: Container(
                    child: SvgPicture.asset('assets/images/logo.svg'))),
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Platform.isIOS
                        ? Icon(CupertinoIcons.add_circled_solid, size: 32)
                        : Icon(Icons.add),
                    color: Color.fromRGBO(64, 130, 188, 1),
                    onPressed: () async {
                      addAccountDialog();
                    },
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAccountsSection() {
    return StreamBuilder<List<Account>>(
        stream: accountBloc.getAccount,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data!.isNotEmpty) {
            final accountsSet = snapshot.data!.toSet();
            return Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: _buildAccountsList(accountsSet.toList()),
              ),
            );
          } else
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Card(
                elevation: 6.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                ),
                child: Platform.isIOS
                    ? CupertinoInkWell(
                        borderRadius: BorderRadius.circular(20),
                        onPressed: () {
                          addAccountDialog();
                        },
                        child: MainAddAccountButton(),
                      )
                    : InkWell(
                        borderRadius: BorderRadius.circular(20),
                        onTap: () async {
                          addAccountDialog();
                        },
                        child: MainAddAccountButton(),
                      ),
              ),
            );
        });
  }

  _buildAccountsList(List<Account> accounts) {
      return SmartRefresher(
        controller: _refreshController,
        onRefresh: () async {
          deadAccs=await fetchRequests();
          accountBloc.fetchAccounts();
          _refreshController.refreshCompleted();
        },
        child: ListView.builder(
            itemCount: accounts.length,
            itemBuilder: (BuildContext context, int index) {
              var account = accounts[index];
              var alertCupertino = CupertinoActionSheet(
                actions: <Widget>[
                  CupertinoActionSheetAction(
                    isDestructiveAction: true,
                    onPressed: () {
                      removeAccount(account);
                      Navigator.of(context).pop();
                    },
                    child: Text('main.account.delete_account'.tr()),
                  ),
                ],
                cancelButton: CupertinoActionSheetAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('access_page.cancel'.tr()),
                ),
              );

              final popupButtonMaterial = PopupMenuButton<int>(
                onSelected: (item) async {
                  removeAccount(account);
                },
                icon: Icon(Icons.more_vert,
                    color: Color.fromRGBO(218, 218, 218, 1)),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: 1,
                    child: Text('main.account.delete_account'.tr()),
                  ),
                ],
              );
              return Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 6.0, horizontal: 20.0),
                child: GestureDetector(
                  child: Card(
                      elevation: 6.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20.0))),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minHeight: 96,
                        ),
                        child: Container(
                          width: double.infinity,
                           padding: const EdgeInsets.only(
                            top:  4.0, right: 0.0),
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Platform.isIOS
                                    ? IconButton(
                                        onPressed: () {
                                          showCupertinoModalPopup(
                                              context: context,
                                              builder: (context) =>
                                                  alertCupertino);
                                        },
                                        icon: Icon(CupertinoIcons.ellipsis,
                                            color:
                                                Color.fromRGBO(218, 218, 218, 1)),
                                      )
                                    : popupButtonMaterial,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Container(
                                      width: 56,
                                      height: 56,
                                      decoration: BoxDecoration(
                                          borderRadius: Platform.isIOS
                                              ? BorderRadius.circular(16.0)
                                              : BorderRadius.circular(2.0),
                                          gradient: LinearGradient(
                                              begin: Alignment.bottomCenter,
                                              end: Alignment.topCenter,
                                              colors: [
                                                Color.fromRGBO(64, 130, 188, 1),
                                                Color.fromRGBO(64, 130, 188, 0.62)
                                              ])),
                                      child: Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                              "${account.scope![0].toUpperCase()}",
                                              style: TextStyle(
                                                  fontSize: 40.0,
                                                  fontFamily: "SFPro",
                                                  fontWeight: FontWeight.w700,
                                                  color: Colors.white),
                                              textAlign: TextAlign.center)),
                                    ),
                                  ),
                                  Flexible(
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      //height: 60,
                                      padding: const EdgeInsets.only(
                                          top:  0.0, right: 30.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [deadAccs.contains(account.accountId)?
                                        Text(brokenAccs.contains(account.accountId)?'main.account.broken_account_short'.tr():'main.account.invalid_account_short'.tr(),
                                            overflow: TextOverflow.ellipsis,
                                            style: GoogleFonts.roboto(
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w700,
                                                color: Color.fromRGBO(
                                                    50, 69, 77, 1))):Container(),
                                          Text("${account.scope}",
                                              overflow: TextOverflow.ellipsis,
                                              style: GoogleFonts.roboto(
                                                  fontSize: 24.0,
                                                  fontWeight: FontWeight.w700,
                                                  color: Color.fromRGBO(
                                                      50, 69, 77, deadAccs.contains(account.accountId)?0.1:1))),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 1.0),
                                            child: Text("${account.identity}",
                                                overflow: TextOverflow.ellipsis,
                                                softWrap: true,
                                                maxLines: 1,
                                                style: GoogleFonts.roboto(
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.w400,
                                                    color: Color.fromRGBO(
                                                        50, 69, 77, deadAccs.contains(account.accountId)?0.1:1))),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      )),
                ),
              );
            }),
      );
   }

  /// Show dialog when click "Add account" btn
  Future<void> addAccountDialog() async {
    switch (await showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: Text('main.camera.add_account_title'.tr(),
                overflow: TextOverflow.ellipsis,
                style: GoogleFonts.roboto(
                    fontStyle: FontStyle.normal,
                    fontSize: 22.0,
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(50, 69, 77, 1))),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "QR");
                },
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Icon(
                        Icons.add_a_photo_outlined,
                        color: Color.fromRGBO(64, 130, 188, 1),
                      ),
                    ),
                    Text('main.account.add_account_scan'.tr(),
                        style: TextStyle(
                            fontFamily: "SFPro",
                            fontSize: 14,
                            fontWeight: FontWeight.w400))
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "Code");
                },
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Icon(
                        Icons.keyboard,
                        color: Color.fromRGBO(64, 130, 188, 1),
                      ),
                    ),
                    Text('main.account.add_account_enter'.tr(),
                        style: TextStyle(
                            fontFamily: "SFPro",
                            fontSize: 14,
                            fontWeight: FontWeight.w400)),
                  ],
                ),
              ),
            ],
          );
        })) {
      case 'QR':
        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => ScanQrScreen(),
            transitionDuration: Duration(seconds: 0)));
        break;
      case 'Code':
        String? deviceToken = await accountBloc.getDeviceToken();

        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) =>
                EnterCodeScreen(deviceToken: deviceToken),
            transitionDuration: Duration(seconds: 0)));
        break;
    }
  }

  Future<void> removeAccount(Account account) async {
    var key = await (securityStorage.read(account.accountId!));
    deadAccs.remove(account.accountId);
    await accountBloc.setDeadAccounts(deadAccs);
    if(key!=null) {
      var sign = getHmacSHA256("${account.accountId}:${account.deviceId}", key);
      accountBloc.deleteAccount(account.accountId, sign, account.deviceId);
    }
    else
      accountBloc.deleteAccount(account.accountId, "", null);
  }

  Future<void> registerAccount(String qrCode) async {
    String requestId = qrCode.split("r=")[1];
    accountBloc.registerAccount(requestId);
  }

  Future<Set<String>> fetchRequests() async {
    var dead= Set<String>();
    var liveAccs=Set<String>();
    var isRequest=false;
    brokenAccs.clear();
    print("REQ BEG");
    try {
        var accounts = await DatabaseProvider.db.getAccounts();
        if (accounts.length > 0) {
          dead= await accountBloc.getDeadAccounts();
          logger.d("DE: ${dead.length}");
          for(int c=0;c<accounts.length;c++) {
            logger.d("AC: ${accounts[c].accountId}");
            if(dead.contains(accounts[c].accountId)) continue;
            var key = await (securityStorage.read(accounts[c].accountId!));

            logger.d("KEY: $key");
            if(key==null) continue;
            var sign = getHmacSHA256(
              "${accounts[c].accountId}:${accounts[c].deviceId}", key);
            liveAccs=await accountBloc.getRequests(accounts[c].accountId, sign,
              accounts[c].deviceId, googleService!, context);
            if(liveAccs.isNotEmpty) {
              if (!liveAccs.contains("dead")) break;
              else dead.add(accounts[c].accountId ?? "");
            }
          }
        //logger.d("LIVE: $liveAccs : ${liveAccs.contains("empty")} ${dead.isNotEmpty}");
          if(liveAccs.isNotEmpty)
            if(!liveAccs.contains("empty") && !liveAccs.contains("dead")) {
              dead.clear();
              isRequest=true;
              for (int c = 0; c < accounts.length; c++)
                if (!liveAccs.contains(accounts[c].accountId))
                  dead.add(accounts[c].accountId ?? "");
            }
          var nAccs=0;
          for(int c=0;c<accounts.length;c++) {
            if (await (securityStorage.read(accounts[c].accountId!)) == null) {
              brokenAccs.add(accounts[c].accountId!);
              dead.add(accounts[c].accountId!);
            }
            if(dead.contains(accounts[c].accountId)) nAccs++;
          }
          if(nAccs==0) dead.clear();
        }
        await accountBloc.setDeadAccounts(dead);
    } catch (e) {
      logger.e("RefreshError $e");
    }
    logger.d("DEAD: ${dead.length}");

    if(!isRequest) {
      print("Clear");
      accountBloc.setAuthRequest(null);
      if (Navigator.canPop(context))
        if(googleService?.isShowing==true || huaweiService?.isShowing==true)
          Navigator.pop(context);
    }
    var regId = await (securityStorage.read("regId"));
    if(regId!=null) {
      //accountBloc.registerAccount(regId);
      accountBloc.fetchAccounts();
      securityStorage.write("regId",null);
      bool wakelockStatus = await Wakelock.enabled;
      logger.d("wakelockStatus: $wakelockStatus");
      if (wakelockStatus) {
        Wakelock.disable();
      }
      Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
    }
    print("REQ END");
    return dead;
  }
}

class EmptyAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  Size get preferredSize => Size(0.0, 0.0);
}
