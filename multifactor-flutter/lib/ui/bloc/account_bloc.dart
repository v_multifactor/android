import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:multifactor/data/database/database.dart';
import 'package:multifactor/data/models/account.dart';
import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/data/models/callback_request.dart';
import 'package:multifactor/data/models/register_code_status_model.dart';
import 'package:multifactor/data/models/register_request.dart';
import 'package:multifactor/domain/interactors/account_interactor.dart';
import 'package:multifactor/domain/google_push_service.dart';
import 'package:multifactor/ui/utils/logger.dart';
import 'package:multifactor/utils/storage/secure_keyvalue_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';

class AccountBloc {
  final securityStorage = SecureKeyValueStorage.instance();

  var accountInteractor = AccountInteractor();
  var _accountFetcher = PublishSubject<List<Account>>();
  var _passwordFetcher = PublishSubject<String?>();
  var _authRequestFetcher = PublishSubject<AccountRequest?>();

  Stream<List<Account>> get getAccount => _accountFetcher.stream;

  Stream<String?> get getPassword => _passwordFetcher.stream;

  Stream<AccountRequest?> get getAuthRequest => _authRequestFetcher.stream;

  bool? isNeedPassword;
  // String deviceToken;

  //  var test;

  void fetchAccounts() async {
    logger.d("FETH");
    var accounts = await accountInteractor.getAccounts();
    if (!_accountFetcher.isClosed) {
      _accountFetcher.sink.add(accounts);
    }
  }

  Future<void> registerAccount(String? requestId) async {
    var accounts = await DatabaseProvider.db.getAccounts();
    String? clientToken = await getDeviceToken();

    var args = RegisterRequest();
    if (accounts.isNotEmpty) {
      args.deviceId = await securityStorage.read("deviceId");
    }
    args.requestId = requestId;
    if (Platform.isAndroid) {
      String? pushPlatform = await getAndroidPushPlatform();
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      args.platformVersion =
          ("${androidInfo.version.release} (${androidInfo.version.sdkInt})");
      args.appVersion = ("${androidInfo.version.codename}");
      args.deviceName = ("${androidInfo.manufacturer} ${androidInfo.model}");
      args.platform = "android";
      args.clientToken = clientToken;
      args.service = pushPlatform;

      var release = androidInfo.version.release;
      var sdkInt = androidInfo.version.sdkInt;
      var manufacturer = androidInfo.manufacturer;
      var model = androidInfo.model;
      logger.d('Android $release (SDK $sdkInt), $manufacturer $model');
    }

    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      args.platformVersion = ("${iosInfo.systemVersion}");
      args.appVersion = ("${iosInfo.utsname}");
      args.deviceName = ("${iosInfo.name}");
      args.platform = "ios";
      args.clientToken = clientToken;
      var systemName = iosInfo.systemName;
      var version = iosInfo.systemVersion;
      var name = iosInfo.name;
      var model = iosInfo.model;
      logger.d('ios $systemName $version, $name $model, ${iosInfo.utsname}');
    }

    await accountInteractor.registerAccount(args);
    fetchAccounts();
  }

  Future<RegisterCodeStatusModel?> sendRegisterCode(
      String code, String? token) async {
    String? pushPlatform = await getAndroidPushPlatform();
    RegisterCodeStatusModel? res =
        await accountInteractor.sendRegisterCode(code, token, pushPlatform);
    return res;
  }

  void approveAuthRequest(CallbackRequest callbackRequest) {
    accountInteractor.approveAuthRequest(callbackRequest);
  }

  void rejectAuthRequest(CallbackRequest callbackRequest) {
    accountInteractor.rejectAuthRequest(callbackRequest);
  }

  void deleteAccount(String? accountId, String sign, String? deviceId) async {

    await accountInteractor.deleteAccount(accountId, sign, deviceId);
    fetchAccounts();
    setAuthRequest(null);
  }

  Future<bool> accountExist(String? accountId) async {
    bool result = await accountInteractor.accountExist(accountId);
    if(result)
      result = await (securityStorage.read(accountId!))!=null;
    return result;
  }

  Future<Set<String>> getRequests(String? accountId, String sign, String? deviceId,
      GooglePushService service, BuildContext context) async {
    var result =
        await (accountInteractor.getRequests(accountId, sign, deviceId));
    logger.d("GET REQUEST $accountId $result");
    if (result == null) return Set<String>();
    if (result.isNotEmpty) {
      if(result[0].accountId=="dead") return {"dead"};
      var lastreq=await securityStorage.read("lastProcReq");
      if(lastreq!=result[0].requestId) {
        service.settingModalBottomSheet(context, result[0]);
        _authRequestFetcher.sink.add(result[0]);
      }
      return result[0].accounts ?? Set<String>();
    }
    return {"empty"};
  }

  Future<String?> getUserPassword() async {
    await getPasswordRequired();
    String? value = await securityStorage.read("userPassword");
    _passwordFetcher.sink.add(value);
    return value;
  }

  void setUserPassword(String password) async {
    await securityStorage.write("userPassword", password);
    _passwordFetcher.sink.add(password);
  }

  void deleteUserPassword() async {
    await securityStorage.write("userPassword", null);
    _passwordFetcher.sink.add(null);
  }

  void setAuthRequest(AccountRequest? accountRequest) async {
    logger.d("accountBloc.getAuthRequest was called $getAuthRequest");
    _authRequestFetcher.sink.add(accountRequest);
  }

  Future<bool> setPasswordRequired(bool isPassNeed) async {
    final prefs = SecureKeyValueStorage.instance();
    await prefs.write('isPassNeed', isPassNeed?'True':'False');
    return true;
  }
  Future<bool> getPasswordRequired() async {
    final storage = SecureKeyValueStorage.instance();
    String result = await storage.read("isPassNeed")??'False';
    isNeedPassword= result.compareTo('True')==0;
    return isNeedPassword??false;
  }
  Future<bool> setDeadAccounts(Set<String> dead) async {
    final prefs = SecureKeyValueStorage.instance();
    await prefs.write('deadAccounts', dead.toString());
    return true;
  }
  Future<Set<String>> getDeadAccounts() async {
    final storage = SecureKeyValueStorage.instance();
    String? result = await storage.read("deadAccounts");
    if(result==null || result=="{}") return Set<String>();
    return Set.from(result.replaceAll(RegExp(r"\{|\}| "),"").split(','));
  }

  Future<bool> setDeviceToken(String? token) async {
    final prefs = SecureKeyValueStorage.instance();
    prefs.write('androidPushClientToken', token);
    return true;
  }

  Future<String?> getDeviceToken() async {
    final storage = SecureKeyValueStorage.instance();
    final prefs = SharedPreferences.getInstance();

    String? result = await storage.read("androidPushClientToken") ??
        (await prefs).getString("androidPushClientToken");

    return result;
  }

  void setAndroidPushPlatform(ServiceTypeEnum serviceType) async {
    final prefs = SecureKeyValueStorage.instance();
    prefs.write('androidPushService', serviceEnumValues.reverse?[serviceType]);
  }

  Future<String?> getAndroidPushPlatform() async {
    final storage = SecureKeyValueStorage.instance();
    final prefs = SharedPreferences.getInstance();
    String? result = await storage.read("androidPushService") ??
        (await prefs).getString("androidPushService");
    if (result == null) {
      return result = serviceEnumValues.reverse![
          ServiceTypeEnum.googlePushService]; // We use google push by default
    }
    logger.d("getAndroidPushPlatform result: $result}");
    return result;
  }

  void biometricsSetPermission(bool permission) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("biometricsPermission", permission);
  }

  Future<bool> biometricsGetPermission() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? result = prefs.getBool("biometricsPermission");

    if (result == null) {
      result = false;
    }
    return result;
  }

  void dispose() {
    _accountFetcher.close();
  }
}
