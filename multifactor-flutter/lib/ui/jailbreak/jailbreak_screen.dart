import 'dart:io';

import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multifactor/ui/utils/logger.dart';

class JailBreakScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return JailBreakState();
  }
}

class JailBreakState extends State<JailBreakScreen> {
  @override
  void initState() {
    super.initState();
    logger.wtf("Jailbreak or Root access has been find.");
  }

  @override
  Widget build(BuildContext context) {
    final permissionName = Platform.isIOS ? "Jailbreak" : "Root";

    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "jailbreak.title".tr(),
              style: GoogleFonts.roboto(
                  fontStyle: FontStyle.normal,
                  fontSize: 22.0,
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(50, 69, 77, 1)),
            ),
            const Padding(padding: const EdgeInsets.all(4)),
            Text(
              "jailbreak.content".tr(namedArgs: {'rule': '$permissionName'}),
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(fontSize: 18),
            ),
            const Padding(padding: const EdgeInsets.all(4)),
            MaterialButton(
              onPressed: () {
                exit(0);
              },
              child: Text(
                "access_page.exit".tr(),
                style: GoogleFonts.roboto(
                    fontStyle: FontStyle.normal,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(50, 69, 77, 1)),
              ),
            )
          ],
        ),
      ),
    );
  }
}
