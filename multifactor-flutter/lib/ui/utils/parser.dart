import 'dart:convert';

import 'package:multifactor/data/models/account_request.dart';
import 'package:multifactor/ui/utils/logger.dart';

List<AccountRequest>? parseListRequests(String responseBody) {
  final parsed = jsonDecode(responseBody)["model"].cast<Map<String, dynamic>>();
  return parsed
      .map<AccountRequest>((json) => AccountRequest.fromJsonForActual(json))
      .toList();
}

AccountRequest parseRequest(Map<dynamic, dynamic> message) {
  var res = AccountRequest.fromJson(message);
  logger.d("res: $res");
  return res;
}
