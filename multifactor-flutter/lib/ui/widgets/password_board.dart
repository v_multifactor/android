import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:local_auth/local_auth.dart';

typedef OnKeyboardClick = Function(BtnTypes type, String content);

enum BtnTypes { NUMBER, BIOMETRIC, CANCEL, BACKSPACE }

class PasswordKeyboard extends StatelessWidget {
  final OnKeyboardClick onButtonClicked;

  final double itemHeight;
  final double width;
  final String cancelText;
  final BiometricType? biometricType;
  final bool isShowBack;
  final bool isEnabled;

  PasswordKeyboard(
      {required this.onButtonClicked,
      required this.cancelText,
      this.itemHeight = 50,
      this.width = 85,
      this.biometricType,
      this.isShowBack = false,
      this.isEnabled = true});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.only(top: 10.0)),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildPasswordItem('1'),
                  _buildPasswordItem('2'),
                  _buildPasswordItem('3'),
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildPasswordItem('4'),
                  _buildPasswordItem('5'),
                  _buildPasswordItem('6'),
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildPasswordItem('7'),
                  _buildPasswordItem('8'),
                  _buildPasswordItem('9'),
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildCancelButton(),
                  _buildPasswordItem('0'),
                  _buildBiometricOrBackspaceBtn(),
                ]),
          ],
        ),
      ),
    );
  }

  Widget _buildPasswordItem(String itemText) {
    return MaterialButton(
      height: itemHeight,
      minWidth: width,
      child: Center(
        child: Text(itemText,
            style: TextStyle(
                fontSize: 36,
                fontWeight: FontWeight.w700,
                color: Color(0xff32454D),
                fontFamily: 'SFPro')),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
      splashColor: Color(0xff2D5DAF).withOpacity(0.05),
      highlightColor: Color(0xff003282).withOpacity(0.07),
      onPressed: !isEnabled
          ? null
          : () {
              onButtonClicked(BtnTypes.NUMBER, itemText);
            },
    );
  }

  Widget _buildBiometricOrBackspaceBtn() {
    if (isShowBack) {
      return MaterialButton(
        height: itemHeight,
        minWidth: width,
        child: Icon(Icons.backspace_outlined),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
        splashColor: Color(0xff2D5DAF).withOpacity(0.05),
        highlightColor: Color(0xff003282).withOpacity(0.07),
        onPressed: !isEnabled
            ? null
            : () {
                onButtonClicked(BtnTypes.BACKSPACE, '');
              },
      );
    }

    if (biometricType == null) {
      return Container(width: width, height: itemHeight);
    }

    return MaterialButton(
      height: itemHeight,
      minWidth: width,
      child: (biometricType == BiometricType.fingerprint)
          ? Icon(Icons.fingerprint)
          : SvgPicture.asset('assets/images/faceid.svg'),
      splashColor: Color(0xff2D5DAF).withOpacity(0.05),
      highlightColor: Color(0xff003282).withOpacity(0.07),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
      onPressed: !isEnabled
          ? null
          : () {
              onButtonClicked(BtnTypes.BIOMETRIC, '');
            },
    );
  }

  Widget _buildCancelButton() {
    return MaterialButton(
      height: itemHeight,
      minWidth: width,
      child: Center(
        child: Text(cancelText,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400)),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
      splashColor: Color(0xff2D5DAF).withOpacity(0.05),
      highlightColor: Color(0xff003282).withOpacity(0.07),
      onPressed: () {
        onButtonClicked(BtnTypes.CANCEL, '');
      },
    );
  }
}
