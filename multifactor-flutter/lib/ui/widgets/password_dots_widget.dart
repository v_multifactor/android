import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PasswordDotsWidget extends StatelessWidget {
  final int count;
  final int selectedCount;
  final double height;
  final double itemPadding;

  PasswordDotsWidget(
      {required this.count,
      required this.selectedCount,
      this.height = 100,
      this.itemPadding = 5});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.all(itemPadding),
        itemCount: count,
        itemBuilder: (_, index) {
          return _buildItem(index < selectedCount);
        },
      ),
    );
  }

  _buildItem(bool isSelected) {
    return Container(
        padding: EdgeInsets.only(left: 2, right: 2),
        child: SvgPicture.asset(isSelected
            ? 'assets/images/PIN-filled.svg'
            : 'assets/images/PIN-empty.svg'));
  }
}
